/* driver_v1.c - Test to check rb's driver_v1 functionality
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#define YCP_FORCE_DEBUG
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "app/test/test_yacup"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Pre-declare required tests here (there is no header for them!) */
int test_xyz_testname(int argc, const char *argv[]);
int test_rb_driver_overwrite(int argc, const char *argv[]);
int test_fsm_driver_simple(int argc, const char *argv[]);
int test_ce_codec_B416K(int argc, const char *argv[]);
int test_ce_command_validate(int argc, const char *argv[]);
int test_ce_command_codec_B416K(int argc, const char *argv[]);
int test_ce_initialization(int argc, const char *argv[]);
int test_ce_driver_faf(int argc, const char *argv[]);
int test_me_basic_io(int argc, const char *argv[]);
int test_me_basic_usage(int argc, const char *argv[]);
int test_me_realistic_campaign(int argc, const char *argv[]);

/** 
 * @brief Define and reserve storage for name-function pairs (NULL-like-ended)
 */
struct test_pair
{
  /**
   * @brief      Test function name
   */
  char *name;

  /**
   * @brief      Test function pointer (standard `main()` footprint)
   */
  int (*fn)(int, const char *[]);
}
test_list[] =
{
  { .name = "test_xyz_testname",           .fn = test_xyz_testname           },
  { .name = "test_rb_driver_overwrite",    .fn = test_rb_driver_overwrite    },
  { .name = "test_fsm_driver_simple",      .fn = test_fsm_driver_simple      },
  { .name = "test_ce_codec_B416K",         .fn = test_ce_codec_B416K         },
  { .name = "test_ce_command_validate",    .fn = test_ce_command_validate    },
  { .name = "test_ce_command_codec_B416K", .fn = test_ce_command_codec_B416K },
  { .name = "test_ce_initialization",      .fn = test_ce_initialization      },
  { .name = "test_ce_driver_faf",          .fn = test_ce_driver_faf          },
  { .name = "test_me_basic_io",            .fn = test_me_basic_io            },
  { .name = "test_me_basic_usage",         .fn = test_me_basic_usage         },
  { .name = "test_me_realistic_campaign",  .fn = test_me_realistic_campaign  },
  { .name = "NULL",                        .fn = NULL                        }
};

/**
 * @brief      The main of the mains
 *
 * @param[in]  argc  The count of arguments
 * @param      argv  The arguments array
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 *
 * @ingroup    tests
 * @version    v1.0.0
 */
int test_yacup(int argc, const char *argv[])
{
  /* Configure _dbg() */
  #define YCP_FNAME "test_yacup"

  _dbg("Hi! from "__FILE__"\n");

  size_t test_idx = 0;
  for (test_idx = 0; test_list[test_idx].fn != NULL; test_idx++)
  {
    _dbg(" ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
    if (test_list[test_idx].fn(argc, argv))
    {
      _dbg("Unexpected ERROR when executing '%s' test!\n",
           test_list[test_idx].name);
      _dbg(" ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
      return 1;
    }
  }
  _dbg(" ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
#undef YCP_FORCE_DEBUG
