/* doxygen-groups.h - Define Doxygen groups structure, not real header
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __DOXYGEN_GROUPS_H
#define __DOXYGEN_GROUPS_H

/* This file covers that groups that are not defined in header files */

/**
 * @defgroup    tests Available tests
 * @{
 *   @brief     This module contains all available tests and all available
 *              debug functions published by the developer
 *   @details   In order to include tests here, use *ingroup* as shown on
 *              template files
 *   @author    CieNTi <cienti@cienti.com>
 * @}
 *
 * @defgroup    debug_functions Debugging functions
 * @{
 *   @brief     Debugging helper functions to ease the development flow
 *   @details   Each *util* or *app* is responsible of create or not a debug
 *              function, but it is recommended to invest a bit of time to
 *              create them, and the more the verbose, the better
 *   @author    CieNTi <cienti@cienti.com>
 * @}
 */

#endif /* __DOXYGEN_GROUPS_H */
