/* string.h - All `me` string-related variables and functions
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_STRING_H
#define __ME_STRING_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * External variables. See source file for complete information
 */

/* Available states strings */
extern char *me_state_str[];

/* Available relational operators strings, short/symbol version */
extern char *me_relation_str[];

/* Available relational operators strings, long/words version */
extern char *me_relation_long_str[];

/* Available logical operators strings */
extern char *me_logic_str[];

/* Available attachment type strings */
extern char *me_attachment_str[];

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup   me_string
 * @{
 */

/**
 * @brief      Create human-like criteria string using *sprintf* approach
 *
 * @param      str           Pointer used to save the generated string
 * @param      me            Pointer to *me* (campaign to be based on)
 * @param      id            request id to search for inside a campaign
 * @param      relation_str  Relational strings to use, default if NULL
 * @param      logic_str     Logical strings to use, default if NULL
 *
 * @return     If successful, the total number of characters written is returned
 *             excluding the null-character appended at the end of the string,
 *             otherwise a negative number is returned in case of failure.
 */
int me_sprint_criteria(char *str, struct me *me, uint64_t id,
                       char *relation_str[], char *logic_str[]);

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_STRING_H */
