/* config.h - Available configuration defines for `me` for yacup project
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_CONFIG_H
#define __ME_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @addtogroup   me_config
 * @{
 */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Maximum length for a *name* field
 */
#define ME_LEN_NAME 96

/**
 * @brief      Maximum length for a *unit* field
 */
#define ME_LEN_UNIT 32

/**
 * @brief      Maximum length for a *location* field
 */
#define ME_LEN_LOCATION 32

/**
 * @brief      Maximum length for an *author* field
 */
#define ME_LEN_AUTHOR 64

/**
 * @brief      Maximum length for a *version* field
 */
#define ME_LEN_VERSION 32

/**
 * @brief      Maximum length for a file name (most common to SOs)
 */
#define ME_LEN_FILE_NAME 255

/**
 * @brief      Maximum length for a file path (most common to SOs)
 */
#define ME_LEN_FILE_PATH 4096

/**
 * @brief      Maximum retries befor returning an error
 */
#define ME_MAX_RETRIES 5

/**
 * @brief      Maximum available pass-checks a measurement can have
 */
#define ME_PASS_CHECKS 4

/**
 * @brief      Maximum available attachments a measurement can have
 */
#define ME_ATTACHMENTS 4

/**
 * @brief      Value used when a temperature was not measured
 */
#define ME_INVALID_TEMPERATURE -275.0

/**
 * @brief      Value used when a humidity was not measured
 */
#define ME_INVALID_HUMIDITY -1.0

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_CONFIG_H */
