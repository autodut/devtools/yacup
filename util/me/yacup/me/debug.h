/* debug.h - Debug functions to ease `me` development flow
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_DEBUG_H
#define __ME_DEBUG_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @addtogroup debug_functions
 * @{
 */

/* C libraries */
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Print `me_meas` information to STDOUT.
 *
 * @param      me    Pointer to a measurement
 * @param      id    Measurement to search for and save to disk
 */
void me_print_meas_info(struct me *me, uint64_t id);

/**
 * @brief      Print `me` information to STDOUT.
 *
 * @param      me    Pointer to a measurements campaign
 */
void me_print_info(struct me *me);

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_DEBUG_H */
