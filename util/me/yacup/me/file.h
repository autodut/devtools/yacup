/* file.h - All `me` file-related variables and functions
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_FILE_H
#define __ME_FILE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @addtogroup   me_file
 * @{
 */

/* C libraries */
#include "yacup/me/config.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Pre-declaration */
struct me_file_driver;

/**
 * @brief      Structure that defines measurement/campaign load/save driver
 * @details    This driver will decide where and how to load/save the data. It
 *             will tipically be a file destination, but it can also be a
 *             hardware interface, like a serial port, so environments without
 *             file systems, like microcontrollers, can still measure and save
 *             data outside of them.
 */
struct me_file_driver
{
  /**
   * @brief      Name for this driver
   */
  char *name;

  /**
   * @brief      Driver specific data, to be managed inside each driver
   */
  void *data;

  /**
   * @brief      Driver specific data length, set by each driver, if apply
   */
  size_t data_len;

  /**
   * @brief      Set to 1 to overwrite file if found. Otherwise any writting
   *             action will be skipped. Will not overwrite by default
   */
  size_t overwrite;

  /**
   * @brief      Appends involved file name to provided slash-ended path
   * @details    This function is in charge of generating a valid driver file
   *             name. Dpending the nature of the driver, the final file will be
   *             a campaign, measurement or other kind of file .. but is what
   *             save and load driver operations will use.
   *
   * @param      str      Pointer used to save the generated string
   * @param      str_len  Length inside str where the last character was written
   * @param      me       Pointer to *me* (campaign to be based on)
   * @param      meas     Pointer to affected measurement
   *
   * @return     If successful, the total number of characters in `str` is
   *             returned excluding the null-character appended at the end of
   *             the string, otherwise a negative number is returned in case of
   *             failure. This allows to overwrite and append using str_len
   */
  int (*path)(char *str, int str_len, struct me *me, struct me_meas *meas);

  /**
   * @brief      Create some file using @ref me and/or @ref me_meas following
   *             this driver rules
   *
   * @param      driver  Pointer to file driver involved in this operation
   * @param      me      Pointer to *me* (campaign to be based on)
   * @param      meas    Pointer to measurement to use
   *
   * @return     One of:
   *             | Value   | Meaning          |
   *             | :----:  | :--------------- |
   *             | `==  0` | Ok               |
   *             | `== -1` | File exists      |
   *             | `>   0` | Error            |
   */
  int (*save)(struct me_file_driver *driver,
              struct me *me,
              struct me_meas *meas);
  
  /**
   * @brief      Read some file using @ref me and/or @ref me_meas following this
   *             driver rules, and make data available to program.
   *
   * @param      driver  Pointer to file driver involved in this operation
   * @param      me      Pointer to *me* (campaign to be based on)
   * @param      meas    Pointer to measurement to use
   *
   * @return     One of:
   *             | Value  | Meaning          |
   *             | :----: | :--------------- |
   *             | `== 0` | Ok               |
   *             | `!= 0` | Error            |
   */
  int (*load)(struct me_file_driver *driver,
              struct me *me,
              struct me_meas *meas);
};

/**
 * @brief      Uses an internal static char array, pre-fills with campaign base
 *             folder (slash-ended), updates length and returns the array
 *             pointer. If error, length will be 0 and returned pointer to array
 *             will be NULL.
 *
 * @param      me     Pointer to *me* (campaign to be based on)
 * @param      f_len  Pointer to length holder to be updated
 *
 * @return     One of:
 *             | Value    | Meaning          |
 *             | :------: | :--------------- |
 *             | `char *` | Ok               |
 *             | `NULL`   | Error            |
 */
char * me_campaign_path(struct me *me, int *f_len);

/**
 * @brief      Get the path to the file used by a driver
 *
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      id      Measurement id to search for inside a campaign
 * @param      driver  Pointer to file driver involved in this operation
 * @param      suffix  Optional suffix to add to string before returning it
 *
 * @return     One of:
 *             | Value    | Meaning          |
 *             | :------: | :--------------- |
 *             | `char *` | Ok               |
 *             | `NULL`   | Error            |
 */
char * me_file_path(struct me *me, uint64_t id,
                    struct me_file_driver *driver, char *suffix);

/**
 * @brief      Save a file following a driver rules
 * @details    Driver will decide the type of file to use, so it will also
 *             decide if @ref me or @ref me_meas are required or not.
 *
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      id      Measurement id to search for inside a campaign
 * @param      driver  Pointer to file driver involved in this operation
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
int me_save(struct me *me, uint64_t id, struct me_file_driver *driver);

/**
 * @brief      Load a file following a driver rules
 * @details    Driver will decide the type of file to use, so it will also
 *             decide if @ref me or @ref me_meas are required or not.
 *
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      id      Measurement id to search for inside a campaign
 * @param      driver  Pointer to file driver involved in this operation
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_load(struct me *me, uint64_t id, struct me_file_driver *driver);

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_FILE_H */
