/* raw_meas.h - Driver for raw measurement files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_FILE_RAW_MEAS_H
#define __ME_FILE_RAW_MEAS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * External variables. See source file for complete information
 */
/* Driver variable */
extern struct me_file_driver raw_meas;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @defgroup   me_file_driver_raw_meas Measurement RAW file (.meas)
 * @{
 *   @ingroup    me_file_driver
 *   @brief      Binary file valid for save and load purposes, apart for being
 *               a valid and safe fallback/default driver
 *   @details    It will use @ref ME_FILE_RAW_MEAS_FILEFORMAT to create
 * measurement file name, and it will be saved inside campaign folder as-is (no
 * subfolders will be used).
 * 
 * Involved measurement data and file will be automatically picked according a
 * provided id and campaign. It is not possible to access measurements outside
 * a campaign.
 * @}
 * 
 * @addtogroup me_file_driver_raw_meas
 * @{
 */

/**
 * @addtogroup me_file_driver_raw_meas_menum Macros and enums
 * @{
 */

/**
 * @brief      Raw measurement files filename format
 */
#define ME_FILE_RAW_MEAS_FILEFORMAT "%08lX.meas"

/** @} */

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_FILE_RAW_MEAS_H */
