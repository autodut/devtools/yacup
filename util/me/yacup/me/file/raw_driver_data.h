/* raw_driver_data.h - Driver for raw measurement files with metadata
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_FILE_RAW_DRIVER_DATA_H
#define __ME_FILE_RAW_DRIVER_DATA_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * External variables. See source file for complete information
 */
/* Driver variable */
extern struct me_file_driver raw_driver_data;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @defgroup   me_file_driver_raw_driver_data Measurement RAW file with extra \
 * \ driver data (.meas + .meas_extra)
 * @{
 *   @ingroup    me_file_driver
 *   @brief      Binary file with extra data valid for save and load purposes,
 *               apart for being a valid and safe fallback/default driver
 *   @details    It will use @ref ME_FILE_RAW_DRIVER_DATA_META_FILEFORMAT to
 * create campaign file name, while will use inner layer driver (@ref raw_meas)
 * file name adding @ref ME_FILE_RAW_DRIVER_DATA_META_SUFFIX suffix. Finally,
 * it will be saved inside campaign folder as-is (no subfolders will be used).
 * 
 * This driver differs with @ref raw_meas in the capability of also save @ref
 * me_file_driver::data both as measurement or as a campaign (useful when a
 * document wants to save some metadata).
 * 
 * Involved measurement data and file will be automatically picked according a
 * provided id and campaign. It is not possible to access measurements outside
 * a campaign.
 * @}
 * 
 * @addtogroup me_file_driver_raw_driver_data
 * @{
 */

/**
 * @addtogroup me_file_driver_raw_driver_data_menum Macros and enums
 * @{
 */

/**
 * @brief      Raw measurement metadata files suffix
 */
#define ME_FILE_RAW_DRIVER_DATA_META_SUFFIX "_meta"

/**
 * @brief      Raw measurement files filename format
 */
#define ME_FILE_RAW_DRIVER_DATA_META_FILEFORMAT "%08lX_raw_driver_data.meta"

/** @} */

/**
 * @addtogroup me_file_driver_raw_driver_data_drv_data Extra document data
 * @{
 */

/**
 * @brief      Size for @ref raw_driver_metadata::operator_name field
 */
#define RAW_DRIVER_DATA_OPERATOR_NAME_SZ 184

/**
 * @brief      Size for @ref raw_driver_metadata::sim_host field
 */
#define RAW_DRIVER_DATA_SIM_HOST_SZ 256

/**
 * @brief      Size for @ref raw_driver_metadata::sim_port field
 */
#define RAW_DRIVER_DATA_SIM_PORT_SZ 8

/**
 * @brief      Size for @ref raw_driver_metadata::ssh_fingerprint field
 */
#define RAW_DRIVER_DATA_SSH_FINGERPRINT_SZ 64

/**
 * @brief      Size for @ref raw_driver_metadata::ssh_priv_id_rsa field
 */
#define RAW_DRIVER_DATA_SSH_PRIV_ID_RSA_SZ 2048

/**
 * @brief      Size for @ref raw_driver_metadata::ssh_pub_id_rsa field
 */
#define RAW_DRIVER_DATA_SSH_PUB_ID_RSA_SZ 512

/**
 * @brief      Size for @ref raw_driver_metadata::comments field
 */
#define RAW_DRIVER_DATA_COMMENTS_SZ 1024

/**
 * @brief      Structure that defines all required data for this specific driver
 * @details    In order to fully fit the struct into memory, pure 2^n size will
 *             be used.
 *
 *             As a first condition, any `brd_comment_` field have to have at
 *             least 1.5 kb to be meaningful into the document.
 *
 *             So sizes will be calculated using the next 2^n value,
 *             substracting the sum of all the fields but comments, and dividing
 *             the value by the amount of comments fields.
 *
 *             Example using the structure of 20200628:
 *
 *             - Sum of fields:           240 bytes
 *             - Comments fields:         2
 *             - Minimum required size:   240 + 2*1500 = 3240
 *             - Next valid 2^n:          4096
 *             - Each comment field size: (4096 - 240) / 2 = 1928
 */
struct raw_driver_metadata
{
  /**
   * @brief      Name of the operator who performed this action
   */
  char operator_name[RAW_DRIVER_DATA_OPERATOR_NAME_SZ];

  /**
   * @brief      Server hostname or IP where the simulation was ran
   */
  char sim_host[RAW_DRIVER_DATA_SIM_HOST_SZ];

  /**
   * @brief      SSH port used to connect to simulation server
   */
  char sim_port[RAW_DRIVER_DATA_SIM_PORT_SZ];

  /**
   * @brief      SSH Fingerprint calculated when SSH keypair was created
   */
  char ssh_fingerprint[RAW_DRIVER_DATA_SSH_FINGERPRINT_SZ];

  /**
   * @brief      SSH `id_rsa` file
   * @warning    SHOULD NOT BE PUBLICLY SHARED!
   */
  char ssh_priv_id_rsa[RAW_DRIVER_DATA_SSH_PRIV_ID_RSA_SZ];

  /**
   * @brief      SSH `id_rsa.pub` file
   * @note       This is the one that can be publicly shared,
   *             while @ref raw_driver_metadata::ssh_priv_id_rsa should
   *             be safely kept out of curious people
   */
  char ssh_pub_id_rsa[RAW_DRIVER_DATA_SSH_PUB_ID_RSA_SZ];

  /**
   * @brief      Miscellaneous comments to add to a measurement or campaign
   */
  char comments[RAW_DRIVER_DATA_COMMENTS_SZ];
};

/** @} */

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_FILE_RAW_DRIVER_DATA_H */
