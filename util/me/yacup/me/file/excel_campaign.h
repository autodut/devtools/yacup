/* excel_campaign.h - Driver for excel campaign files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_FILE_EXCEL_CAMPAIGN_H
#define __ME_FILE_EXCEL_CAMPAIGN_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * External variables. See source file for complete information
 */
/* Driver variable */
extern struct me_file_driver excel_campaign;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @defgroup   me_file_driver_excel_campaign Campaign spreadsheet (.xlsx)
 * @{
 *   @ingroup    me_file_driver
 *   @brief      Excel 2007 compatible spreadsheet containing all campaign
 *               measurements data
 *   @details    It will use ref ME_FILE_EXCEL_CAMPAIGN_FILEFORMAT to create
 * campaign file name, and it will be saved inside campaign folder as-is (no
 * subfolders will be used).
 * 
 * Involved measurement data and file will be automatically picked according a
 * provided id and campaign. It is not possible to access measurements outside
 * a campaign.
 *   @note       Please read about @ref libxlsxwriter requirements in order to
 *               correctly use this function
 * @}
 * 
 * @addtogroup me_file_driver_excel_campaign
 * @{
 */

/* C libraries */
#include <xlsxwriter.h>

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_excel_campaign_menum Macros and enums
 * @{
 */

/**
 * @brief      Excel campaign files filename format
 */
#define ME_FILE_EXCEL_CAMPAIGN_FILEFORMAT "%08lX_measurements-table.xlsx"

/**
 * @brief      Maximum length when composing Excel cell with this driver
 */
#define ME_EXCEL_MAX_CELL_LEN 512

/**
 * @brief      Criteria types for human-like text output in Excel files
 */
enum me_excel_criteria
{
  /**
   * @brief      Print short criteria (symbols) string in generated documents
   */
  EXCEL_SHORT_CRITERIA = 0x00,
  /**
   * @brief      Print long criteria (words) string in generated documents
   */
  EXCEL_LONG_CRITERIA  = 0x01
};

/** @} */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @name       Extra functions
 * @{
 */

/**
 * @brief      Fill a given Excel sheet with current campaign measurements data
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      meas  Pointer to affected measurement
 * @param      wb    *libxlsxwriter* workbook to use for Excel file handling
 * @param      ws    *libxlsxwriter* worksheet to use for Excel file handling
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_excel_add_campaign_sheet(struct me *me, struct me_meas *meas,
                                lxw_workbook *wb, lxw_worksheet *ws);

/** @} */

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_FILE_EXCEL_CAMPAIGN_H */
