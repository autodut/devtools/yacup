/* md_meas.h - Driver for Markdown measurement files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_FILE_MARKDOWN_H
#define __ME_FILE_MARKDOWN_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * External variables. See source file for complete information
 */
/* Available states strings */
extern char *md_state_str[];

/* Available relational operators strings, short/symbol version */
extern char *md_relation_str[];

/* Available relational operators strings, long/words version */
extern char *md_relation_long_str[];

/* Available logical operators strings */
extern char *md_logic_str[];

/* Available attachment type strings */
extern char *md_attachment_str[];

/* Driver variable */
extern struct me_file_driver md_meas;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @defgroup   me_file_driver_md_meas Measurement details (.md)
 * @{
 *   @ingroup    me_file_driver
 *   @brief      Markdown file containing all measurement details in human-like
 *               format, ready to be processed by Pandoc
 *   @details    It will use @ref ME_FILE_MD_MEAS_FILEFORMAT to create
 * measurement file name, and it will be saved inside campaign folder as-is (no
 * subfolders will be used).
 * 
 * Involved measurement data and file will be automatically picked according a
 * provided id and campaign. It is not possible to access measurements outside
 * a campaign.
 * @}
 * 
 * @addtogroup me_file_driver_md_meas
 * @{
 */

/**
 * @addtogroup me_file_driver_md_meas_menum Macros and enums
 * @{
 */

/**
 * @brief      Markdown measurement files filename format
 */
#define ME_FILE_MD_MEAS_FILEFORMAT "%08lX.md"

/**
 * @brief      Maximum length when composing Markdown line with this driver
 */
#define ME_MD_MAX_LINE_LEN 512

/**
 * @brief      Criteria types for human-like text output in Markdown files
 */
enum me_md_criteria
{
  /**
   * @brief      Print short criteria (symbols) string in generated documents
   */
  MD_SHORT_CRITERIA = 0x00,
  /**
   * @brief      Print long criteria (words) string in generated documents
   */
  MD_LONG_CRITERIA  = 0x01
};

/** @} */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @name       Extra functions
 * @{
 */

/**
 * @brief      Create Markdown table string with campaign measurements table
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      str   Pointer used to save the generated string
 * @param      crit  Select long or short criteria. See @ref me_md_criteria
 *
 * @return     If successful, the total number of characters written is returned
 *             excluding the null-character appended at the end of the string,
 *             otherwise a negative number is returned in case of failure.
 */
int me_md_table_string(struct me *me, char *str, enum me_md_criteria crit);

/** @} */

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_FILE_MARKDOWN_H */
