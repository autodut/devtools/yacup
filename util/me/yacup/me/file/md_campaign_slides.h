/* md_campaign_slides.h - Driver for markdown campaign beamer slides files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_FILE_MD_CAMPAIGN_SLIDES_H
#define __ME_FILE_MD_CAMPAIGN_SLIDES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * External variables. See source file for complete information
 */
/* Driver variable */
extern struct me_file_driver md_campaign_slides;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @defgroup   me_file_driver_md_campaign_slides Campaign beamer slides (.md)
 * @{
 *   @ingroup    me_file_driver
 *   @brief      Markdown slides following Beamer formatting with campaign
 *               measurements details, ready to be processed by Pandoc
 *   @details    It will use @ref ME_FILE_MD_CAMPAIGN_SLIDES_FILEFORMAT to
 * create campaign file name, and it will be saved inside campaign folder as-is
 * (no subfolders will be used).
 * 
 * Involved measurement data and file will be automatically picked according a
 * provided id and campaign. It is not possible to access measurements outside
 * a campaign.
 * @}
 * 
 * @addtogroup me_file_driver_md_campaign_slides
 * @{
 */

/**
 * @addtogroup me_file_driver_md_campaign_slides_menum Macros and enums
 * @{
 */

/**
 * @brief      Markdown campaign beamer slides files filename format
 */
#define ME_FILE_MD_CAMPAIGN_SLIDES_FILEFORMAT "%08lX_measurements-slides.md"

/** @} */

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_FILE_MD_CAMPAIGN_SLIDES_H */
