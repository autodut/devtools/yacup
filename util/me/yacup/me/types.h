/* types.h - Available enums/types for `me` for yacup project
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_TYPES_H
#define __ME_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @addtogroup   me_api
 * @{
 */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Available measurement states
 * @remark     This enum is handled as **uint8_t** when read from/saved to disk
 */
enum me_state
{
  /**
   * @brief      Not done yet
   */
  ME_STA_NOT_DONE = 0x00,
  /**
   * @brief      Is already done
   */
  ME_STA_DONE     = 0x01,
  /**
   * @brief      For some reason, is skipped
   */
  ME_STA_SKIPPED  = 0x02,
  /**
   * @brief      Errors when retrieving measurement
   */
  ME_STA_ERROR    = 0x03
};

/**
 * @brief      Available relational operators
 * @remark     This enum is handled as **uint8_t** when read from/saved to disk
 */
enum me_relation
{
  /**
   * @brief      Default. Undefined. It will always validate
   */
  ME_REL_ANYTHING         = 0x00,
  /**
   * @brief      Value have to be equal to the reference
   * @warning    This comparison can fail but apparently be valid for an human.
   *             This is due to binary differences between 2 floating-point
   *             values where 1.000000000001 is not the same as 1.000000000002
   */
  ME_REL_EQUAL            = 0x01,
  /**
   * @brief      Value have to be different than the reference
   */
  ME_REL_NOT_EQUAL        = 0x02,
  /**
   * @brief      Value have to be greater than the reference
   */
  ME_REL_GREATER          = 0x03,
  /**
   * @brief      Value have to be less than the reference
   */
  ME_REL_LESS             = 0x04,
  /**
   * @brief      Value have to be greater than or equal to the reference
   */
  ME_REL_GREATER_OR_EQUAL = 0x05,
  /**
   * @brief      Value have to be less than or equal to the reference
   */
  ME_REL_LESS_OR_EQUAL    = 0x06
};

/**
 * @brief      Available logical operators
 * @remark     This enum is handled as **uint8_t** when read from/saved to disk
 */
enum me_logic
{
  /**
   * @brief      Default. Condition have to be true, and there is no more
   */
  ME_LOG_NONE = 0x00,
  /**
   * @brief      This and the next condition have to be true
   */
  ME_LOG_AND  = 0x01,
  /**
   * @brief      This or the next condition have to be true
   */
  ME_LOG_OR   = 0x02
};

/**
 * @brief      Available logical attachments types
 * @remark     This enum is handled as **uint8_t** when read from/saved to disk
 */
enum me_attachment
{
  /**
   * @brief      Default. There is no attachment
   */
  ME_ATT_NO     = 0x00,
  /**
   * @brief      An custom attachment. Same writer will have a specific reader
   */
  ME_ATT_CUSTOM = 0x01,
  /**
   * @brief      An attachment composed by a series of values of type double
   */
  ME_ATT_ARRAY  = 0x02,
  /**
   * @brief      An image attachment of type PNG
   */
  ME_ATT_PNG    = 0x03,
  /**
   * @brief      An image attachment of type JPG
   */
  ME_ATT_JPG    = 0x04,
  /**
   * @brief      An image attachment of type BMP
   */
  ME_ATT_BMP    = 0x05
};

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_TYPES_H */
