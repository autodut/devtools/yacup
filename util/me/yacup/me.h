/* me.h - Measurement engine API for yacup project
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ME_H
#define __ME_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup me Measurement Engine
 * @{
 *   @brief      Automate tests where measurements/samples are involved
 *   @details    > **TODO**: details here
 *   
 *   ### Requirements when Excel spreadsheet are involved @anchor libxlsxwriter
 *   
 *   Driver @ref excel_campaign requires `libxlsxwriter` to generate Excel
 *   spreadsheets.
 *   
 *   The following procedure was tested on a Debian 10 OS, so it may vary on
 *   others systems. Please follow it in order to install the library:
 *   
 *   ```bash
 *   sudo apt-get install -y zlib1g-dev
 *   git clone https://github.com/jmcnamara/libxlsxwriter.git
 *   cd libxlsxwriter
 *   make
 *   sudo make install
 *   sudo ldconfig
 *   ```
 *   
 *   @todo       Write a description/\@details
 *   @ingroup    util
 *   @author     CieNTi <cienti@cienti.com>
 *
 *   @defgroup   me_api Measurements Interface
 *   @{
 *     @brief      Complete API documentation for measurements setup, sampling
 *                 functions and samples handling and its properties
 *     @author     CieNTi <cienti@cienti.com>
 *   @}
 *
 *   @defgroup   me_string Strings interface
 *   @{
 *     @brief      Exported strings, strings arrays and functions related to
 *                 strings generation and management. Useful when working with
 *                 human output, like documents
 *     @author     CieNTi <cienti@cienti.com>
 *   @}
 *
 *   @defgroup   me_file File/Documents Interface
 *   @{
 *     @brief      Complete API documentation to work with files by using the
 *                 provided interface
 *     @author     CieNTi <cienti@cienti.com>
 *   @}
 *
 *   @defgroup   me_config Configuration
 *   @{
 *     @brief      Documentation for used macros
 *     @author     CieNTi <cienti@cienti.com>
 *   @}
 *
 *   @defgroup   me_file_driver Available file drivers
 *   @{
 *     @brief      Already made and tested file drivers to export/import
 *                 measurements and/or campaigns in different file formats
 *     @author     CieNTi <cienti@cienti.com>
 *   @}
 *
 *   @defgroup   me_campaign Available campaigns
 *   @{
 *     @brief      Already made and tested full measurements procedures
 *     @details    Each ME campaign found here can be instantiated by calling
 *                 its init function.
 *     @author     CieNTi <cienti@cienti.com>
 *   @}
 * @}
 * 
 * @addtogroup   me_api
 * @{
 */

/* C libraries */
#include <stdint.h>
#include <stddef.h>
#include "yacup/me/types.h"
#include "yacup/me/config.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Pre-declaration */
struct me;

/**
 * @brief      Structure that defines a measurement setup action
 * @details    A measurement setup is whatever is needed to do to be ready to
 *             perform the measurement itself
 */
struct me_setup
{
  /**
   * @brief      Numeric identifier for this setup
   */
  uint64_t id;
  /**
   * @brief      Name for this setup
   */
  char *name;
  /**
   * @brief      Function to call to actually perform the real setup
   */
  int (*action)(struct me *me, size_t m_idx);
};

/**
 * @brief      Structure that defines a measuring action
 * @details    A request is whatever is needed to do, to actually retrieve the
 *             measurement value, in terms of "a quantity with and unit that is
 *             obtained by comparing it with a reference quantity and unit"
 */
struct me_request
{
  /**
   * @brief      Numeric identifier for this measuring action
   */
  uint64_t id;
  /**
   * @brief      Name for this measuring action
   */
  char *name;
  /**
   * @brief      Function to call to actually measure and get the measurement
   */
  int (*action)(struct me *me, size_t m_idx);
};

/**
 * @brief      Structure that defines a condition a measurement need to pass
 * @details    If *type* is not defined as @ref ME_REL_ANYTHING, the value of
 *             the measurement will be tested against the reference value,
 *             using the configured pass condition. If next is not defined as
 *             @ref ME_LOG_NONE, next pass-check will be also evaluated by
 *             applying the selected logical operator.
 */
struct me_pass_check
{
  /**
   * @brief      Type of relation between the measurement and reference values
   */
  enum me_relation type;
  /**
   * @brief      Value used as reference for comparisons
   */
  double reference;
  /**
   * @brief      Type of logic operation to perform with the next condition
   */
  enum me_logic next;
};

/**
 * @brief      Structure that defines a measurement
 * @details    A measurement is compound by its value, unit, as much as the
 *             environment as possible and whatever is needed to correctly
 *             identify it and trace/track it
 */
struct me_meas
{
  /**
   * @brief      Unique identifier for this measurement inside its campaign
   */
  uint64_t id;
  /**
   * @brief      Holds the state of this measurement
   */
  enum me_state state;
  /**
   * @brief      Name. Shown where a human is involved (gui, doc, ...)
   */
  char name[ME_LEN_NAME];
  /**
   * @brief      Symbol/Variable/Short names. Use plain-text, Markdown or LaTeX
   */
  char symbol[ME_LEN_NAME];
  /**
   * @brief      Measurement units. Use plain-text, Markdown or LaTeX
   */
  char unit[ME_LEN_UNIT];
  /**
   * @brief      Conditions to pass for this measurement to be valid
   */
  struct me_pass_check pass_if[ME_PASS_CHECKS];
  /**
   * @brief      Extra files attached to this measurement
   */
  enum me_attachment attachment[ME_ATTACHMENTS];
  /**
   * @brief      Identifier of the measurements campaign (not filled by user)
   */
  uint64_t campaign;
  /**
   * @brief      Identifier of the setup to execute before measuring
   */
  uint64_t setup;
  /**
   * @brief      Identifier of the request to execute to get the measurement
   */
  uint64_t request;
  /**
   * @brief      Measured location (location, net, connection, pin, ...)
   */
  char location[ME_LEN_LOCATION];
  /**
   * @brief      Channel used to measure (0-based)
   */
  uint64_t channel;
  /**
   * @brief      Timestamp of this measurement in milliseconds
   */
  uint64_t timestamp;
  /**
   * @brief      Measured value
   */
  double value;
  /**
   * @brief      Temperature in Celsius at which the measurement was taken
   */
  double temperature;
  /**
   * @brief      Humidity in percentage at which the measurement was taken
   */
  double humidity;
  /**
   * @brief      Generic data managed by others than `me` itself
   */
  void *metadata;
  /**
   * @brief      Length for the generic data managed by others than `me` itself
   */
  void *metadata_len;
};

/**
 * @brief      Structure that defines measurements interactions callbacks
 * @details    Structure that defines all possible interactions callbacks the 
 *             user can control during the execution of a setup, request and
 *             value saving procedure
 */
struct me_user_cb
{
  /**
   * @brief      Called before to setup the measurement
   * @details    If not provided (set it to NULL), setup will be directly called
   *             with no waits nor tweaks.
   *
   * @param      me     Pointer to *me* (campaign to be based on)
   * @param      m_idx  Measurement index to use (Use @ref me_locate_meas_by_id)
   *
   * @return     One of:
   *             | Value  | Meaning                                       |
   *             | :----: | :-------------------------------------------- |
   *             | `== 0` | Ok. It will continue to 'setup' step          |
   *             | `!= 0` | Error. It will stop the measurement procedure |
   */
  int (*setup_prepare)(struct me *me, size_t m_idx);
  /**
   * @brief      Called if measurement setup fails
   * @details    If not provided (set it to NULL) and an error is found, a
   *             direct error will be returned
   *
   * @param      me     Pointer to *me* (campaign to be based on)
   * @param      m_idx  Measurement index to use (Use @ref me_locate_meas_by_id)
   *
   * @return     One of:
   *             | Value  | Meaning                                       |
   *             | :----: | :-------------------------------------------- |
   *             | `== 0` | Ok. Try again                                 |
   *             | `<  0` | Skip. Will accept this step as if valid       |
   *             | `>  0` | Error. It will stop the measurement procedure |
   */
  int (*setup_retry)(struct me *me, size_t m_idx);
  /**
   * @brief      Called before to request the measurement
   * @details    If not provided (set it to NULL), request will be directly
   *             called with no waits nor tweaks.
   *
   * @param      me     Pointer to *me* (campaign to be based on)
   * @param      m_idx  Measurement index to use (Use @ref me_locate_meas_by_id)
   *
   * @return     One of:
   *             | Value  | Meaning                                       |
   *             | :----: | :-------------------------------------------- |
   *             | `== 0` | Ok. It will continue to 'setup' step          |
   *             | `!= 0` | Error. It will stop the measurement procedure |
   */
  int (*request_prepare)(struct me *me, size_t m_idx);
  /**
   * @brief      Called if measurement request fails
   * @details    If not provided (set it to NULL) and an error is found, a
   *             direct error will be returned
   *
   * @param      me     Pointer to *me* (campaign to be based on)
   * @param      m_idx  Measurement index to use (Use @ref me_locate_meas_by_id)
   *
   * @return     One of:
   *             | Value  | Meaning                                       |
   *             | :----: | :-------------------------------------------- |
   *             | `== 0` | Ok. Try again                                 |
   *             | `<  0` | Skip. Will accept this step as if valid       |
   *             | `>  0` | Error. It will stop the measurement procedure |
   */
  int (*request_retry)(struct me *me, size_t m_idx);
  /**
   * @brief      Called before to save the measurement
   * @details    If not provided (set it to NULL), save will be directly called
   *             with no waits nor tweaks.
   *
   * @param      me     Pointer to *me* (campaign to be based on)
   * @param      m_idx  Measurement index to use (Use @ref me_locate_meas_by_id)
   *
   * @return     One of:
   *             | Value  | Meaning                                       |
   *             | :----: | :-------------------------------------------- |
   *             | `== 0` | Ok. It will continue to 'setup' step          |
   *             | `!= 0` | Error. It will stop the measurement procedure |
   */
  int (*save_prepare)(struct me *me, size_t m_idx);
  /**
   * @brief      Called if there is an error while saving a measurement to file
   * @details    If not provided (set it to NULL) and an error is found, a
   *             direct error will be returned
   *
   * @param      me     Pointer to *me* (campaign to be based on)
   * @param      m_idx  Measurement index to use (Use @ref me_locate_meas_by_id)
   *
   * @return     One of:
   *             | Value  | Meaning                                       |
   *             | :----: | :-------------------------------------------- |
   *             | `== 0` | Ok. Try again                                 |
   *             | `<  0` | Skip. Will accept this step as if valid       |
   *             | `>  0` | Error. It will stop the measurement procedure |
   */
  int (*save_retry)(struct me *me, size_t m_idx);
};

/**
 * @brief      Structure that defines a measurement
 * @details    A measurement is compound by its value, unit, as much as the
 *             environment as possible and whatever is needed to correctly
 *             identify it and trace/track it
 */
struct me
{
  /**
   * @brief      Unique identifier for this measurements campaign
   */
  uint64_t id;
  /**
   * @brief      Name. Shown where a human is involved (gui, doc, ...)
   */
  char *name;
  /**
   * @brief      Path to add to the working folder to read from/save to files
   */
  char *folder;
  /**
   * @brief      Timestamp of this measurement in milliseconds
   */
  uint64_t timestamp;
  /**
   * @brief      Pointer to file driver for automatic save/load operations
   * @details    It is an optional parameter, setting it to NULL will disable
   *             automatic features (user need to manually save the measurement
   *             after retrieving it if it is expected to be preserved).
   */
  struct me_file_driver *file_driver;
  /**
   * @brief      Author name
   */
  char author[ME_LEN_NAME];
  /**
   * @brief      Version string (avoid prefix it with 'v', just semver)
   */
  char version[ME_LEN_NAME];
  /**
   * @brief      If set to 1, a measurement will be performed ignoring if it was
   *             previously taken. Otherwise, no action will be taken (setup,
   *             request, callbacks, ...)
   */
  int repeat_measurement;
  /**
   * @brief      If set to 1, setup will be called ignoring the previous setup.
   *             Otherwise, if a setup and the previous one are the same, setup
   *             step will be skipped (both setup preparation and setup itself)
   */
  int repeat_setup;
  /**
   * @brief      In order to avoid repetitions, '0' cannot be used as it can be
   *             valid both for identifier and index, so setup pointer is used
   *             instead as it cannot be valid and NULL.
   */
  struct me_setup *last_setup_bck;
  /**
   * @brief      Pointer to array of pointers of valid setups
   */
  struct me_setup **setup;
  /**
   * @brief      Index of the last used setup
   */
  size_t last_setup_idx;
  /**
   * @brief      Pointer to array of pointers of valid requests
   */
  struct me_request **request;
  /**
   * @brief      Index of the last used request
   */
  size_t last_request_idx;
  /**
   * @brief      Pointer to array of pointers of valid measurements
   */
  struct me_meas **meas;
  /**
   * @brief      Index of the last used measurement
   */
  size_t last_meas_idx;
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Validates if a `me` is full, partial or not valid at all
 * @details    A fully defined `me` (campaign) contains the full triad: setups,
 *             requests and measurements definitions with memory storage. It is
 *             not complete, but neiter invalid at some situations, if there is
 *             not setups and requests, but measurements list is complete. This
 *             situation appears when a campaign is defined at runtime or load
 *             from disk, where setups and/or request are not still available.
 *             User need to keep this track, otherwise a lot of segmentation
 *             faults can arise. *You have been warned*
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Fully valid      |
 *             | `<  0` | Partially valid  |
 *             | `>  0` | Invalid          |
 */
int me_validate(struct me *me);

/**
 * @brief      Initializes a measurement engine referenced by a `me` pointer
 * @details    Checks and initializes `me` common data, then calls the lower
 *             level init function passed by argument.
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_init(struct me *me);

/**
 * @brief      Create a valid timestamp (epoch in ms)
 *
 * @param      timestamp  Pointer to holder to be written with the timestamp
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_timestamp_ms(uint64_t *timestamp);

/**
 * @brief      Locate a measurement inside a campaign by its id
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      id    Measurement id to search for inside a campaign
 * @param      m_idx Pointer to holder for index inside array, once located
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_locate_meas_by_id(struct me *me, uint64_t id, size_t *m_idx);

/**
 * @brief      Locate a setup inside a campaign by its id
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      id    setup id to search for inside a campaign
 * @param      s_idx Pointer to holder for index inside array, once located
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_locate_setup_by_id(struct me *me, uint64_t id, size_t *s_idx);

/**
 * @brief      Locate a request inside a campaign by its id
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      id    request id to search for inside a campaign
 * @param      r_idx Pointer to holder for index inside array, once located
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_locate_request_by_id(struct me *me, uint64_t id, size_t *r_idx);

/**
 * @brief      Evaluates all applicable measurement conditions
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      id    Measurement id to search for inside a campaign
 * @param      pass  Pointer to holder for evaluated value (1 true / 0 false)
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
int me_eval_meas(struct me *me, uint64_t id, int *pass);

/**
 * @brief      Performs a measurement (setup, request and save value)
 * @details    Optional callbacks, if provided, will be used in the process of
 *             setup (prepare, retry), request (prepare, retry) and save a
 *             measurement (prepare, retry). This pairs will leave open the
 *             possibility for a user to tweak each part of the process.
 *
 * @param      me    Pointer to *me* (campaign to be based on)
 * @param      id    Measurement id to search for inside a campaign
 * @param      cb    Pointer to structure of interaction callbacks
 *
 * @return     One of:
 *             | Value  | Meaning                                             |
 *             | :----: | :-------------------------------------------------- |
 *             | `== 0` | Ok. Measurement was performed and saved to file     |
 *             | `<  0` | Partial Ok. Measurement was performed but not saved |
 *             | `>  0` | Error. Measurement failed and it was not saved      |
 */
int me_perform_meas(struct me *me, uint64_t id, struct me_user_cb *cb);

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ME_H */
