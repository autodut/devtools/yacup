/* test_me_basic_usage.c - Test *me* basic/common usage flow
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"
#include "yacup/me/debug.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#define YCP_FORCE_DEBUG
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/test/test_me_basic_usage"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Get references of what is in me/campaign/simple/campaign.c */
extern struct me campaign_simple;
/* Get references of what is in me/campaign/simple/callbacks.c */
extern struct me_user_cb campaign_simple_cb;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Test *me* basic/common usage flow
 *
 * @param[in]  argc  The count of arguments
 * @param      argv  The arguments array
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 *
 * @ingroup    tests
 * @version    v1.0.0
 */
int test_me_basic_usage(int argc, const char * argv[])
{
  /* Configure _dbg() */
  #define YCP_FNAME "test_me_basic_usage"

  (void)argc;
  (void)argv;

  _dbg("Hi! from "__FILE__"\n");

  /* Variables related to the test itself */
  size_t loop_idx = 0;
  int pass_res = 0;
  #define M_VARS 4
  /* The following 3 arrays are COMPLETELY related to 'campaign_simple' */
  uint64_t t_meas[M_VARS] = { 0xDEAF0001, 0xDEAF0002, 0xDEAF0003, 0xDEAF0004 };
  int error_table[M_VARS] = {          0,          1,          1,          1 };
  int pass_table[M_VARS]  = {          1,          0,          1,          0 };

  /* Init */
  _dbg("Initializing a campaign\n");
  if (me_init(&campaign_simple))
  {
    _dbg("Cannot initialize a measurements campaign\n");
    return 1;
  }
  _dbg("Measurements campaign '%s' initialized successfully\n",
       campaign_simple.name);

  /* Iterate all our required measurements */
  for (loop_idx = 0; loop_idx < M_VARS; loop_idx++)
  {
    /* Print measurement information before the operation */
    me_print_meas_info(&campaign_simple, t_meas[loop_idx]);

    /* Lets try a setup with success and no callbacks */
    _dbg("Perform meas: Lets try a setup with success and no callbacks\n");
    if (me_perform_meas(&campaign_simple,
                        t_meas[loop_idx],
                        &campaign_simple_cb))
    {
      _dbg("Cannot perform measurement '0x%08lX'\n", t_meas[loop_idx]);

      /* Expected error by test? */
      if (error_table[loop_idx] == 1)
      {
        _dbg("Expected fail, continue\n");
      }
      else
      {
        _dbg("Unexpected fail. ERROR\n");
        return 1;
      }
    }

    if (me_eval_meas(&campaign_simple, t_meas[loop_idx], &pass_res))
    {
      _dbg("Error when evaluating the measurement\n");
    }

    _dbg("Measurement performed successfully. Returned value: %.2f\n",
         campaign_simple.meas[campaign_simple.last_meas_idx]->value);

    /* Print measurement information after the operation */
    me_print_meas_info(&campaign_simple, t_meas[loop_idx]);

    /* Expected pass by test? */
    if (pass_table[loop_idx] == pass_res)
    {
      _dbg("Expected criteria result, continue\n");
    }
    else
    {
      _dbg("Unexpected criteria result. ERROR\n");
      return 1;
    }
    _dbg(" ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
  }

  /* Cya! */
  _dbg("If you are reading this, everything went correctly :_)\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
#undef YCP_FORCE_DEBUG
