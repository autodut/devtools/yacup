/* test_me_realistic_campaign.c - Test *me* usage on a more realistic scenario
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"
#include "yacup/me/file.h"
#include "yacup/me/file/raw_meas.h"
#include "yacup/me/file/md_meas.h"
#include "yacup/me/file/md_campaign_slides.h"
#include "yacup/me/file/excel_campaign.h"
#include "yacup/me/file/raw_driver_data.h"
#include "yacup/me/debug.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#define YCP_FORCE_DEBUG
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/test/test_me_realistic_campaign"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Get references of what is in cb/me/campaign/realistic/campaign.c */
extern struct me campaign_realistic;
extern struct me_user_cb campaign_realistic_cb;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Test *me* usage on a more realistic scenario
 *
 * @param[in]  argc  The count of arguments
 * @param      argv  The arguments array
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 *
 * @ingroup    tests
 * @version    v1.0.0
 */
int test_me_realistic_campaign(int argc, const char * argv[])
{
  /* Configure _dbg() */
  #define YCP_FNAME "test_me_realistic_campaign"

  (void)argc;
  (void)argv;

  _dbg("Hi! from "__FILE__"\n");

  /* Variables related to the test itself */
  size_t loop_idx = 0;

  /* Init */
  _dbg("Initializing a campaign\n");
  if (me_init(&campaign_realistic))
  {
    _dbg("Cannot initialize a measurements campaign\n");
    return 1;
  }
  _dbg("Measurements campaign '%s' initialized successfully\n",
       campaign_realistic.name);

  /* Assign 'Raw measurement' file driver for auto-saving */
  campaign_realistic.file_driver = &raw_meas;

  /* Iterate all measurements found */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg(" - 0x%08lX ------------------------------------------------------\n",
         campaign_realistic.meas[loop_idx]->id);

    /* Lets try a setup with success and no callbacks */
    _dbg("Perform meas: Lets try a setup with success and no callbacks\n");
    if (me_perform_meas(&campaign_realistic,
                        campaign_realistic.meas[loop_idx]->id,
                        &campaign_realistic_cb))
    {
      _dbg("Cannot perform measurement '0x%08lX'\n",
           campaign_realistic.meas[loop_idx]->id);
      return 1;
    }

    _dbg("Measurement performed successfully. Returned value: %.2f\n",
         campaign_realistic.meas[campaign_realistic.last_meas_idx]->value);

    /* Print measurement information after the operation */
    me_print_meas_info(&campaign_realistic,
                       campaign_realistic.meas[loop_idx]->id);
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Reached this point, all 16 measurements should be saved already.
   * We are going to retrieve all the measurements from disk in order to
   * populate the list. To check the data is read correctly, we loop now the
   * list erasing some data */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Deleting '0x%08lX' value, setup and request\n",
         campaign_realistic.meas[loop_idx]->id);
    campaign_realistic.meas[loop_idx]->value = 0.0;
    campaign_realistic.meas[loop_idx]->setup = 0;
    campaign_realistic.meas[loop_idx]->request = 0;
  }

  /* Print the last one as a control point */
  me_print_meas_info(&campaign_realistic,
                     campaign_realistic.meas[loop_idx - 1]->id);

  /* Loop again, reading from file to array this time */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Reading '0x%08lX' measurement from '.meas' file\n",
         campaign_realistic.meas[loop_idx]->id);

    /* Load a measurement */
    if (me_load(&campaign_realistic,
                campaign_realistic.meas[loop_idx]->id,
                &raw_meas))
    {
      _dbg("Cannot load the measurement '0x%08lX'\n",
           campaign_realistic.meas[loop_idx]->id);
      return 1;
    }

    /* If everything load correctly, none of the following can be 0 */
    if (((campaign_realistic.meas[loop_idx]->value   <  0.001) &
         (campaign_realistic.meas[loop_idx]->value   > -0.001)) ||
        (campaign_realistic.meas[loop_idx]->setup   == 0)      ||
        (campaign_realistic.meas[loop_idx]->request == 0))
    {
      _dbg("Something went wrong after reading from a file\n");
      return 1;
    }
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Use Markdown measurement driver to store measurements */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Saving '0x%08lX' measurement to '%s' Markdown file\n",
         campaign_realistic.meas[loop_idx]->id,
         me_file_path(&campaign_realistic,
                      campaign_realistic.meas[loop_idx]->id,
                      &md_meas,
                      NULL));

    /* Save a measurement */
    if (me_save(&campaign_realistic,
                campaign_realistic.meas[loop_idx]->id,
                &md_meas) > 0)
    {
      _dbg("Cannot save the measurement '0x%08lX'\n",
           campaign_realistic.meas[loop_idx]->id);
      return 1;
    }
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Use Markdown campaign beamer slides driver to store campaign */
  _dbg("Saving '0x%08lX' campaign to '%s' Markdown beamer slides file\n",
       campaign_realistic.id,
       me_file_path(&campaign_realistic,
                    0,
                    &md_campaign_slides,
                    NULL));

  /* Save a measurement */
  if (me_save(&campaign_realistic,
              0,
              &md_campaign_slides) > 0)
  {
    _dbg("Cannot save the campaign '0x%08lX'\n", campaign_realistic.id);
    return 1;
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Use Excel campaign driver to store campaign */
  _dbg("Saving '0x%08lX' campaign to '%s' Excel campaign file\n",
       campaign_realistic.id,
       me_file_path(&campaign_realistic,
                    0,
                    &excel_campaign,
                    NULL));

  /* Save a measurement */
  if (me_save(&campaign_realistic,
              0,
              &excel_campaign) > 0)
  {
    _dbg("Cannot save the campaign '0x%08lX'\n", campaign_realistic.id);
    return 1;
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Use raw_driver_data driver to store measurements */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Saving '0x%08lX' measurement to '%s' raw_driver_data file\n",
         campaign_realistic.meas[loop_idx]->id,
         me_file_path(&campaign_realistic,
                      campaign_realistic.meas[loop_idx]->id,
                      &raw_driver_data,
                      NULL));

    /* Save a measurement */
    if (me_save(&campaign_realistic,
                campaign_realistic.meas[loop_idx]->id,
                &raw_driver_data) > 0)
    {
      _dbg("Cannot save raw_driver_data measurement '0x%08lX'\n",
           campaign_realistic.meas[loop_idx]->id);
      return 1;
    }
  }

  /* Use raw_driver_data driver to store campaign */
  _dbg("Saving '0x%08lX' campaign to '%s' raw_driver_data campaign file\n",
       campaign_realistic.id,
       me_file_path(&campaign_realistic,
                    0,
                    &raw_driver_data,
                    NULL));

  /* Save a campaign */
  if (me_save(&campaign_realistic,
              0,
              &raw_driver_data) != 0)
  {
    _dbg("Error saving raw_driver_data campaign '0x%08lX'. Expected OK\n",
         campaign_realistic.id);
    return 1;
  }

  /* Save a campaign again (overwrite is ON, so it should just save) */
  if (me_save(&campaign_realistic,
              0,
              &raw_driver_data) != 0)
  {
    _dbg("Error saving raw_driver_data campaign '0x%08lX'. Expected OK\n",
         campaign_realistic.id);
    return 1;
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Assign 'Raw measurement + metadata' file driver for auto-saving */
  campaign_realistic.file_driver = &raw_driver_data;
  raw_driver_data.overwrite = 0;

  /* Try to perform all measurements again, but only if not already done */
  campaign_realistic.repeat_measurement = 0;

  /* As all are already performed, force the last one to not-done */
  campaign_realistic.meas[15]->state = ME_STA_ERROR;
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Performing meas '0x%08lX': Will not be repeated if already done\n",
         campaign_realistic.meas[loop_idx]->id);

    /* Will not be repeated if already done */
    if (me_perform_meas(&campaign_realistic,
                        campaign_realistic.meas[loop_idx]->id,
                        &campaign_realistic_cb))
    {
      _dbg("Cannot perform measurement '0x%08lX'\n",
           campaign_realistic.meas[loop_idx]->id);
      return 1;
    }

    _dbg("Measurement performed successfully. Returned value: %.2f\n",
         campaign_realistic.meas[campaign_realistic.last_meas_idx]->value);
  }

  /* Save a campaign again (overwrite is OFF, so it should return -1) */
  if (me_save(&campaign_realistic,
              0,
              &raw_driver_data) != -1)
  {
    _dbg("Error saving raw_driver_data campaign '0x%08lX'. Expected EXISTS\n",
         campaign_realistic.id);
    return 1;
  }

  /* Restore setting */
  campaign_realistic.repeat_measurement = 1;
  raw_driver_data.overwrite = 1;

  /* Save a campaign again (overwrite is ON, so it should just save) */
  if (me_save(&campaign_realistic,
              0,
              &raw_driver_data) != 0)
  {
    _dbg("Error saving raw_driver_data campaign '0x%08lX'. Expected OK\n",
         campaign_realistic.id);
    return 1;
  }
  _dbg(" -----------------------------------------------------------------\n");

  /* Reached this point, all 16 measurements should be saved already.
   * We are going to retrieve all the measurements from disk in order to
   * populate the list. To check the data is read correctly, we loop now the
   * list erasing some data */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Deleting '0x%08lX' value, setup and request\n",
         campaign_realistic.meas[loop_idx]->id);
    campaign_realistic.meas[loop_idx]->value = 0.0;
    campaign_realistic.meas[loop_idx]->setup = 0;
    campaign_realistic.meas[loop_idx]->request = 0;
  }

  /* Print the last one as a control point */
  me_print_meas_info(&campaign_realistic,
                     campaign_realistic.meas[loop_idx - 1]->id);

  /* Loop again, reading from file to array this time */
  for (loop_idx = 0; campaign_realistic.meas[loop_idx] != NULL; loop_idx++)
  {
    _dbg("Reading '0x%08lX' measurement from '.meas' file\n",
         campaign_realistic.meas[loop_idx]->id);

    /* Load a measurement */
    if (me_load(&campaign_realistic,
                campaign_realistic.meas[loop_idx]->id,
                &raw_driver_data))
    {
      _dbg("Cannot load the measurement '0x%08lX'\n",
           campaign_realistic.meas[loop_idx]->id);
      return 1;
    }

    /* If everything load correctly, none of the following can be 0 */
    if (((campaign_realistic.meas[loop_idx]->value   <  0.001) &
         (campaign_realistic.meas[loop_idx]->value   > -0.001)) ||
        (campaign_realistic.meas[loop_idx]->setup   == 0)      ||
        (campaign_realistic.meas[loop_idx]->request == 0))
    {
      _dbg("Something went wrong after reading from a file\n");
      return 1;
    }
  }
  /* Restore settings */
  campaign_realistic.file_driver = &raw_meas;
  _dbg(" -----------------------------------------------------------------\n");

  /* Print campaign information */
  me_print_info(&campaign_realistic);

  /* Cya! */
  _dbg("If you are reading this, everything went correctly :_)\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
#undef YCP_FORCE_DEBUG
