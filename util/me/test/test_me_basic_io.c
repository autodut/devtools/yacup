/* test_me_basic_io.c - Test *me* file I/O capabilities
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"
#include "yacup/me/file.h"
#include "yacup/me/file/raw_meas.h"
#include "yacup/me/debug.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#define YCP_FORCE_DEBUG
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/test/test_me_basic_io"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Get references of what is in me/campaign/simple/campaign.c */
extern struct me campaign_simple;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @brief      Test *me* file I/O capabilities
 *
 * @param[in]  argc  The count of arguments
 * @param      argv  The arguments array
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 *
 * @ingroup    tests
 * @version    v1.0.0
 */
int test_me_basic_io(int argc, const char * argv[])
{
  /* Configure _dbg() */
  #define YCP_FNAME "test_me_basic_io"

  (void)argc;
  (void)argv;

  _dbg("Hi! from "__FILE__"\n");

  /* Variables related to the test itself */
  uint64_t t_meas_01 = 0xDEAF0001;
  uint64_t t_meas_02 = 0xDEAF0002;
  size_t idx = 0;
  size_t meas_idx = 0;
  char *a_pt = NULL;
  char *b_pt = NULL;
  struct me_meas source_meas = { 0x00 };
  int eval_res = 0;

  _dbg("Initializing a campaign\n");
  if (me_init(&campaign_simple))
  {
    _dbg("Cannot initialize measurements campaign '%s'\n",
         (campaign_simple.name != NULL)?campaign_simple.name:"no name");
    return 1;
  }
  _dbg("Measurements campaign '%s' initialized successfully\n",
       campaign_simple.name);

  /* Assign 'Raw measurement' file driver for auto-saving */
  campaign_simple.file_driver = &raw_meas;

  _dbg("Saving measurement 0x%08lX\n", t_meas_01);
  if (me_save(&campaign_simple, t_meas_01, &raw_meas) > 0)
  {
    _dbg("Cannot save measurements 0x%08lX\n", t_meas_01);
    return 1;
  }
  _dbg("Measurements 0x%08lX saved successfully\n", t_meas_01);

  /* Print measurement information */
  me_print_meas_info(&campaign_simple, t_meas_01);

  _dbg("Saving measurement 0x%08lX\n", t_meas_02);
  if (me_save(&campaign_simple, t_meas_02, &raw_meas) > 0)
  {
    _dbg("Cannot save measurements 0x%08lX\n", t_meas_02);
    return 1;
  }
  _dbg("Measurements 0x%08lX saved successfully\n", t_meas_02);

  /* Print measurement information */
  me_print_meas_info(&campaign_simple, t_meas_02);

  /* Print campaign information */
  me_print_info(&campaign_simple);

  /* Now read one measurement, but in order to be able to compare it, we need
   * to save a copy */

  /* Locate the measurement */
  meas_idx = 0;
  if (me_locate_meas_by_id(&campaign_simple, t_meas_02, &meas_idx))
  {
    _dbg("Cannot locate the measurement '0x%08lX'\n", t_meas_02);
    return 1;
  }

  /* Copy its current value */
  a_pt = (char *)&source_meas;
  b_pt = (char *)campaign_simple.meas[meas_idx];
  for (idx = 0; idx < sizeof(struct me_meas); idx++)
  {
    /* Copy and delete current value */
    a_pt[idx] = b_pt[idx];
    b_pt[idx] = 0;
  }
  _dbg("Measurement '0x%08lX' preserved and deleted successfully\n",
       t_meas_02);

  /* Measurement is just erased, so is not possible to find it. Check it */
  if (me_load(&campaign_simple, t_meas_02, &raw_meas) == 0)
  {
    _dbg("Measurement '0x%08lX' is still there, why? ERROR\n", t_meas_02);
    return 1;
  }

  /* Keep the id, or it will be not possible to re-find it */
  campaign_simple.meas[meas_idx]->id = t_meas_02;

  /* Load a measurement */
  if (me_load(&campaign_simple, t_meas_02, &raw_meas))
  {
    _dbg("Cannot load the measurement '0x%08lX'\n", t_meas_02);
    return 1;
  }

  /* Compare previous and loaded values */
  for (idx = 0; idx < sizeof(struct me_meas); idx++)
  {
    /* Compare both me_meas byte by byte */
    if (a_pt[idx] != b_pt[idx])
    {
      _dbg("Measurement mismatch at index %lu for '0x%08lX'\n",
           idx,
           t_meas_02);
      return 1;
    }
  }
  _dbg("Measurement '0x%08lX' loaded successfully\n", t_meas_02);

  /* Print measurement information */
  me_print_meas_info(&campaign_simple, t_meas_02);


  /* ~ Test single-condition measurement ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  _dbg("Testing single-condition meas. (0 > 1 should fail)\n");
  if (me_eval_meas(&campaign_simple, t_meas_02, &eval_res))
  {
    _dbg("Error when evaluating '0x%08lX' conditions. ERROR\n", t_meas_02);
    return 1;
  }
  if (eval_res == 1)
  {
    _dbg("Measurement '0x%08lX' unexpectedly PASSED. ERROR\n", t_meas_02);
    return 1;
  }
  _dbg("Measurement '0x%08lX' NOT PASSED, as expected\n", t_meas_02);

  /* Locate measurement to be able to tweak it */
  meas_idx = 0;
  if (me_locate_meas_by_id(&campaign_simple, t_meas_02, &meas_idx))
  {
    _dbg("Cannot locate the measurement '0x%08lX'\n", t_meas_02);
    return 1;
  }

  /* Create a fake value, this time should pass */
  campaign_simple.meas[meas_idx]->value = 2;

  /* Test single-condition measurement */
  _dbg("Continue cond. with test (2 > 1 should pass)\n");
  if (me_eval_meas(&campaign_simple, t_meas_02, &eval_res))
  {
    _dbg("Error when evaluating '0x%08lX' conditions. ERROR\n", t_meas_02);
    return 1;
  }
  if (eval_res == 0)
  {
    _dbg("Measurement '0x%08lX' unexpectedly NOT PASSED. ERROR\n", t_meas_02);
    return 1;
  }
  _dbg("Measurement '0x%08lX' PASSED, as expected\n", t_meas_02);

  /* Return changed value to its initial value */
  campaign_simple.meas[meas_idx]->value = 0;


  /* ~ Test double-condition measurement ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  _dbg("Testing double-condition meas. ((0 > 1) && (0 < 1000) should fail)\n");
  if (me_eval_meas(&campaign_simple, t_meas_01, &eval_res))
  {
    _dbg("Error when evaluating '0x%08lX' conditions. ERROR\n", t_meas_01);
    return 1;
  }
  if (eval_res == 1)
  {
    _dbg("Measurement '0x%08lX' unexpectedly PASSED. ERROR\n", t_meas_01);
    return 1;
  }
  _dbg("Measurement '0x%08lX' NOT PASSED, as expected\n", t_meas_01);

  /* Locate measurement to be able to tweak it */
  meas_idx = 0;
  if (me_locate_meas_by_id(&campaign_simple, t_meas_01, &meas_idx))
  {
    _dbg("Cannot locate the measurement '0x%08lX'\n", t_meas_01);
    return 1;
  }

  /* Create a fake value, this time should not pass because higher value */
  campaign_simple.meas[meas_idx]->value = 1001;

  /* Test double-condition measurement */
  _dbg("Continue cond. with test ((1001 > 1) && (1001 < 1000) should fail)\n");
  if (me_eval_meas(&campaign_simple, t_meas_01, &eval_res))
  {
    _dbg("Error when evaluating '0x%08lX' conditions. ERROR\n", t_meas_01);
    return 1;
  }
  if (eval_res == 1)
  {
    _dbg("Measurement '0x%08lX' unexpectedly PASSED. ERROR\n", t_meas_01);
    return 1;
  }
  _dbg("Measurement '0x%08lX' NOT PASSED, as expected\n", t_meas_01);

  /* Create a fake value, this time should pass */
  campaign_simple.meas[meas_idx]->value = 2;

  /* Test double-condition measurement */
  _dbg("Continue cond. with test ((2 > 1) && (2 < 1000) should pass)\n");
  if (me_eval_meas(&campaign_simple, t_meas_01, &eval_res))
  {
    _dbg("Error when evaluating '0x%08lX' conditions. ERROR\n", t_meas_01);
    return 1;
  }
  if (eval_res == 0)
  {
    _dbg("Measurement '0x%08lX' unexpectedly NOT PASSED. ERROR\n", t_meas_01);
    return 1;
  }
  _dbg("Measurement '0x%08lX' PASSED, as expected\n", t_meas_01);

  /* Return changed value to its initial value */
  campaign_simple.meas[meas_idx]->value = 0;

  /* Cya! */
  _dbg("If you are reading this, everything went correctly :_)\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
#undef YCP_FORCE_DEBUG
