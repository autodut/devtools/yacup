/* meas.c - Measurement definitions for *realistic* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/realistic/meas"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Array of pointers to measurements
 */
struct me_meas *campaign_realistic_meas[] =
{
  /* ID 0xA00B0100 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0100, .setup = 0xA0000001, .request = 0xB0000001,
    .name = "Average voltage", .symbol = "V_{avg}", .unit = "V",
    .channel = 0, .location = "TP001",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 2.9,  .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 3.3,  .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0200 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0200, .setup = 0xA0000001, .request = 0xB0000002,
    .name = "Maximum voltage", .symbol = "V_{max}", .unit = "V",
    .channel = 0, .location = "TP001",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_LESS_OR_EQUAL, .reference = 3.7, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0300 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0300, .setup = 0xA0000001, .request = 0xB0000003,
    .name = "Minimum voltage", .symbol = "V_{min}", .unit = "V",
    .channel = 0, .location = "TP001",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER_OR_EQUAL, .reference = 2.5, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0400 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0400, .setup = 0xA0000001, .request = 0xB0000004,
    .name = "Frequency", .symbol = "f", .unit = "Hz",
    .channel = 0, .location = "TP001",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_ANYTHING, .reference = 0,   .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0500 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0500, .setup = 0xA0000002, .request = 0xB0000001,
    .name = "Average voltage", .symbol = "V_{avg}", .unit = "V",
    .channel = 1, .location = "TP002",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 2.9,  .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 3.3,  .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0600 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0600, .setup = 0xA0000002, .request = 0xB0000002,
    .name = "Maximum voltage", .symbol = "V_{max}", .unit = "V",
    .channel = 1, .location = "TP002",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_LESS_OR_EQUAL, .reference = 3.7, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0700 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0700, .setup = 0xA0000002, .request = 0xB0000003,
    .name = "Minimum voltage", .symbol = "V_{min}", .unit = "V",
    .channel = 1, .location = "TP002",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER_OR_EQUAL, .reference = 2.5, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0800 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0800, .setup = 0xA0000002, .request = 0xB0000004,
    .name = "Frequency", .symbol = "f", .unit = "Hz",
    .channel = 1, .location = "TP002",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_ANYTHING, .reference = 0,   .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B0900 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B0900, .setup = 0xA0000003, .request = 0xB0000001,
    .name = "Average voltage", .symbol = "V_{avg}", .unit = "V",
    .channel = 2, .location = "TP003",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 2.9,  .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 3.3,  .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1000 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1000, .setup = 0xA0000003, .request = 0xB0000002,
    .name = "Maximum voltage", .symbol = "V_{max}", .unit = "V",
    .channel = 2, .location = "TP003",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_LESS_OR_EQUAL, .reference = 3.7, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1100 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1100, .setup = 0xA0000003, .request = 0xB0000003,
    .name = "Minimum voltage", .symbol = "V_{min}", .unit = "V",
    .channel = 2, .location = "TP003",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER_OR_EQUAL, .reference = 2.5, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1200 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1200, .setup = 0xA0000003, .request = 0xB0000004,
    .name = "Frequency", .symbol = "f", .unit = "Hz",
    .channel = 2, .location = "TP003",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_ANYTHING, .reference = 0,   .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1300 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1300, .setup = 0xA0000004, .request = 0xB0000001,
    .name = "Average voltage", .symbol = "V_{avg}", .unit = "V",
    .channel = 3, .location = "TP004",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 2.9,  .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 3.3,  .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1400 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1400, .setup = 0xA0000004, .request = 0xB0000002,
    .name = "Maximum voltage", .symbol = "V_{max}", .unit = "V",
    .channel = 3, .location = "TP004",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_LESS_OR_EQUAL, .reference = 3.7, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1500 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1500, .setup = 0xA0000004, .request = 0xB0000003,
    .name = "Minimum voltage", .symbol = "V_{min}", .unit = "V",
    .channel = 3, .location = "TP004",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER_OR_EQUAL, .reference = 2.5, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xA00B1600 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xA00B1600, .setup = 0xA0000004, .request = 0xB0000004,
    .name = "Frequency", .symbol = "f", .unit = "Hz",
    .channel = 3, .location = "TP004",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_ANYTHING, .reference = 0,   .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* List ending ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  NULL
};

#undef YCP_NAME
