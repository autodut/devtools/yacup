/* campaign.c - Full definition for *realistic* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/realistic/campaign"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Get references of what is in the sidecar files:
 * - setup.c
 * - request.c
 * - meas.c
 */
extern struct me_setup *campaign_realistic_setup[];
extern struct me_request *campaign_realistic_request[];
extern struct me_meas *campaign_realistic_meas[];

/**
 * @defgroup me_campaign_realistic Realistic campaign for advanced tests
 * @{
 *   @ingroup    me_campaign
 *   @brief      Campaign used to develop advanced functions and files drivers
 *   @details    Under this environment is where the identifiers for each setup,
 *               request or measurement have to be unique (each one in its own
 *               array, of course). This campaign contains:
 * 
 * - 4x setups, one per measurement location: TP001 to TP004 (setup id:
 *   `0xA0000001` to `0xA0000004`). All of them will exit with success
 * - 4x requests, one per type of measurement: average, minimum, maximum and
 *   frequency (request id: `0xB0000001` to `0xB0000004`). Value will be created
 *   based on the measurement channel. See the source for extra information. All
 *   of them will exit with success
 * - 16x measurements (mixing 4x setups + 4x requests)
 * 
 * ### How to use the campaign
 * 
 * First of all, compile all files belonging to `me/campaign/realistic`.
 * 
 * This campaign has no headers, so in order to be able to use it, it is needed
 * to use an `extern` like the following in your code:
 * 
 * ```C
 * // Get references of what is in me/campaign/realistic/campaign.c
 * extern struct me campaign_realistic;
 * ```
 * 
 * ### How to use the callbacks
 * 
 * Additionally, a set of callbacks is provided just for completeness. All will
 * exit with success. Similar to the previous, use something like:
 * 
 * ```C
 * // Get references of what is in me/campaign/realistic/callbacks.c
 * extern struct me_user_cb campaign_realistic_cb;
 * ```
 * @}
 *
 * @addtogroup me_campaign_realistic
 * @{
 */

/**
 * @name Campaign
 * @{
 */

/**
 * @brief      Realistic campaign for advanced tests
 * @details    The 'me' element is the campaign itself, as it entirely defines a
 *             full measurements campaign.
 */
struct me campaign_realistic =
{
  .id = 0xC0000001,
  .name = "A Realistic Campaign",
  .folder = "campaign_realistic",
  .author = "CieNTi",
  .version = "1.2.0",
  .repeat_measurement = 1,
  .repeat_setup = 1,
  .setup = campaign_realistic_setup,
  .request = campaign_realistic_request,
  .meas = campaign_realistic_meas
};

/** @} */

/** @} */

#undef YCP_NAME
