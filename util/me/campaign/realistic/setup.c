/* setup.c - Setup definitions for *realistic* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/realistic/setup"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Setups (Whatever is needed before to measure)
 */
/* Test setup action function that succeess */
int campaign_realistic_0xA0000001_setup_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xA0000001_setup_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Executing setup for TP001 location\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test setup action function that succeess */
int campaign_realistic_0xA0000002_setup_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xA0000002_setup_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Executing setup for TP002 location\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test setup action function that succeess */
int campaign_realistic_0xA0000003_setup_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xA0000003_setup_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Executing setup for TP003 location\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test setup action function that succeess */
int campaign_realistic_0xA0000004_setup_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xA0000004_setup_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Executing setup for TP004 location\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Array of pointers to setups
 */
struct me_setup *campaign_realistic_setup[] =
{
  &(struct me_setup)
  {
    .id = 0xA0000001, .name = "Setup for test TP001 location. Exit with OK",
    .action = campaign_realistic_0xA0000001_setup_action
  },
  &(struct me_setup)
  {
    .id = 0xA0000002, .name = "Setup for test TP002 location. Exit with OK",
    .action = campaign_realistic_0xA0000002_setup_action
  },
  &(struct me_setup)
  {
    .id = 0xA0000003, .name = "Setup for test TP003 location. Exit with OK",
    .action = campaign_realistic_0xA0000003_setup_action
  },
  &(struct me_setup)
  {
    .id = 0xA0000004, .name = "Setup for test TP004 location. Exit with OK",
    .action = campaign_realistic_0xA0000004_setup_action
  },
  NULL
};

#undef YCP_NAME
