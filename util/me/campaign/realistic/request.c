/* request.c - Requests definitions for *realistic* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/realistic/request"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Requests (Whatever is needed to get the valued to measure)
 */
/* Test request action function that succeess */
int campaign_realistic_0xB0000001_request_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xB0000001_request_action"

  /* Create a new value. To add variability, channel will be used  */
  me->meas[m_idx]->value = 3.1 * ((me->meas[m_idx]->channel + 2.0) / 4.0);

  /* Cya! */
  _dbg("Executing request for 'Average voltage'\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test request action function that succeess */
int campaign_realistic_0xB0000002_request_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xB0000002_request_action"

  /* Create a new value. To add variability, channel will be used  */
  me->meas[m_idx]->value = 3.7 * ((me->meas[m_idx]->channel + 2.0) / 4.0);

  /* Cya! */
  _dbg("Executing request for 'Maximum voltage'\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test request action function that succeess */
int campaign_realistic_0xB0000003_request_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xB0000003_request_action"

  /* Create a new value. To add variability, channel will be used  */
  me->meas[m_idx]->value = 2.5 * ((me->meas[m_idx]->channel + 2.0) / 4.0);

  /* Cya! */
  _dbg("Executing request for 'Minimum voltage'\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test request action function that succeess */
int campaign_realistic_0xB0000004_request_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_realistic_0xB0000004_request_action"

  /* Create a new value. To add variability, channel will be used  */
  me->meas[m_idx]->value = 50.0 * ((me->meas[m_idx]->channel + 2.0) / 4.0);

  /* Cya! */
  _dbg("Executing request for 'Frequency'\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Array of pointers to requests
 */
struct me_request *campaign_realistic_request[] =
{
  &(struct me_request)
  {
    .id = 0xB0000001, .name = "Request for 'Average voltage'. Exit with OK",
    .action = campaign_realistic_0xB0000001_request_action
  },
  &(struct me_request)
  {
    .id = 0xB0000002, .name = "Request for 'Maximum voltage'. Exit with OK",
    .action = campaign_realistic_0xB0000002_request_action
  },
  &(struct me_request)
  {
    .id = 0xB0000003, .name = "Request for 'Minimum voltage'. Exit with OK",
    .action = campaign_realistic_0xB0000003_request_action
  },
  &(struct me_request)
  {
    .id = 0xB0000004, .name = "Request for 'Frequency'. Exit with OK",
    .action = campaign_realistic_0xB0000004_request_action
  },
  NULL
};

#undef YCP_NAME
