/* meas.c - Measurement definitions for *simple* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/simple/meas"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Array of pointers to measurements
 */
struct me_meas *campaign_simple_meas[] =
{
  /* ID 0xDEAF0001 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xDEAF0001, .setup = 0xA0000001, .request = 0xB0000001,
    .name = "Files count", .symbol = "F0", .unit = "files",
    .channel = 0, .location = "$HOME/f0",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 1,    .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 1000, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xDEAF0002 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xDEAF0002, .setup = 0xA0000001, .request = 0xB0000002,
    .name = "Files count", .symbol = "F1", .unit = "files",
    .channel = 1, .location = "$HOME/f1",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 1,    .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 1000, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xDEAF0003 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xDEAF0003, .setup = 0xA0000002, .request = 0xB0000001,
    .name = "Files count", .symbol = "F2", .unit = "files",
    .channel = 2, .location = "$HOME/f2",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 1,    .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 1000, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* ID 0xDEAF0004 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  &(struct me_meas)
  {
    .id = 0xDEAF0004, .setup = 0xA0000002, .request = 0xB0000002,
    .name = "Files count", .symbol = "F3", .unit = "files",
    .channel = 3, .location = "$HOME/f3",
    .state = ME_STA_NOT_DONE, .pass_if =
    {
      { .type = ME_REL_GREATER, .reference = 1,    .next = ME_LOG_AND  },
      { .type = ME_REL_LESS,    .reference = 1000, .next = ME_LOG_NONE }
    },
    .temperature = ME_INVALID_TEMPERATURE, .humidity = ME_INVALID_HUMIDITY,
    .attachment = { ME_ATT_NO }
  },
  /* List ending ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  NULL
};

#undef YCP_NAME
