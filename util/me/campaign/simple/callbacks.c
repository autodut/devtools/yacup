/* callbacks.c - Sample callbacks for *simple* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/simple/callbacks"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Create some callbacks */

/* Called before to setup the measurement. 0 Ok, otherwise Error */
static int cb_setup_prepare(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "cb_setup_prepare"

  (void)me;
  (void)m_idx;

  _dbg("Preparing a setup\n");

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Called if measurement setup fails. 0 Ok, -1 Skip, 1 Error */
static int cb_setup_retry(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "cb_setup_retry"

  (void)me;
  (void)m_idx;

  _dbg("Retrying a setup\n");

  /* Let's go! */
  return -1;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Called before to request the measurement. 0 Ok, otherwise Error */
static int cb_request_prepare(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "cb_request_prepare"

  (void)me;
  (void)m_idx;

  _dbg("Preparing a request\n");

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Called if measurement request fails. 0 Ok, -1 Skip, 1 Error */
static int cb_request_retry(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "cb_request_retry"

  (void)me;
  (void)m_idx;

  _dbg("Retrying a request\n");

  /* Let's go! */
  return -1;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Called before to save the measurement. 0 Ok, otherwise Error */
static int cb_save_prepare(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "cb_save_prepare"

  (void)me;
  (void)m_idx;

  _dbg("Preparing to save\n");

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Called if error while saving measurement to file. 0 Ok, -1 Skip, 1 Error */
static int cb_save_retry(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "cb_save_retry"

  (void)me;
  (void)m_idx;

  _dbg("Retrying to save\n");

  /* Let's go! */
  return -1;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Create the struct later used on tests
 */
struct me_user_cb campaign_simple_cb =
{
  .setup_prepare   = cb_setup_prepare,
  .setup_retry     = cb_setup_retry,
  .request_prepare = cb_request_prepare,
  .request_retry   = cb_request_retry,
  .save_prepare    = cb_save_prepare,
  .save_retry      = cb_save_retry
};

#undef YCP_NAME
