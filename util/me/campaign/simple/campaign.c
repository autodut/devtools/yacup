/* campaign.c - Full definition for *simple* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/simple/campaign"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Get references of what is in the sidecar files:
 * - setup.c
 * - request.c
 * - meas.c
 */
extern struct me_setup *campaign_simple_setup[];
extern struct me_request *campaign_simple_request[];
extern struct me_meas *campaign_simple_meas[];

/**
 * @defgroup me_campaign_simple Simple campaign for basic tests
 * @{
 *   @ingroup    me_campaign
 *   @brief      Campaign used to develop basic functions and interface
 *   @details    Under this environment is where the identifiers for each setup,
 *               request or measurement have to be unique (each one in its own
 *               array, of course). This campaign contains:
 * 
 * - 1x setup that exits with success (setup id: `0xA0000001`)
 * - 1x setup that exits with fail (setup id: `0xA0000002`)
 * - 1x request that set the measurement value to `10.05` and exits with success
 *   (request id: `0xB0000001`)
 * - 1x request that exits with fail (request id: `0xB0000002`)
 * - 4x measurements (mixing 2x setups + 2x requests)
 * 
 * ### How to use the campaign
 * 
 * First of all, compile all files belonging to `me/campaign/simple`.
 * 
 * This campaign has no headers, so in order to be able to use it, it is needed
 * to use an `extern` like the following in your code:
 * 
 * ```C
 * // Get references of what is in me/campaign/simple/campaign.c
 * extern struct me campaign_simple;
 * ```
 * 
 * ### How to use the callbacks
 * 
 * Additionally, a set of callbacks is provided to check the correct engine
 * behaviour, where `xxx_prepare()` exits with success and `xxx_retry()` exits
 * with 'skip' code (-1). Similar to the previous, use something like:
 * 
 * ```C
 * // Get references of what is in me/campaign/simple/callbacks.c
 * extern struct me_user_cb campaign_simple_cb;
 * ```
 * @}
 *
 * @addtogroup me_campaign_simple
 * @{
 */

/**
 * @name Campaign
 * @{
 */

/**
 * @brief      Simple campaign for basic testing purposes
 * @details    The 'me' element is the campaign itself, as it entirely defines a
 *             full measurements campaign.
 */
struct me campaign_simple =
{
  .id = 0xC0000001,
  .name = "A Simple Campaign",
  .folder = "campaign_simple",
  .author = "CieNTi",
  .version = "1.2.0",
  .repeat_measurement = 1,
  .repeat_setup = 1,
  .setup = campaign_simple_setup,
  .request = campaign_simple_request,
  .meas = campaign_simple_meas
};

/** @} */

/** @} */

#undef YCP_NAME
