/* request.c - Requests definitions for *simple* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/simple/request"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Requests (Whatever is needed to get the valued to measure)
 */
/* Test request action function that succeess */
int campaign_simple_0xB0000001_request_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_simple_0xB0000001_request_action"

  /* Just some valid value for this measurement */
  me->meas[m_idx]->value = 10.05;

  /* Cya! */
  _dbg("Request is executed and will exit with Ok\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test request action function that fails */
int campaign_simple_0xB0000002_request_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_simple_0xB0000002_request_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Request is executed and will exit with Error\n");
  return 1;

  /* Free _dbg() config */
  #undef YCP_FNAME
}
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Array of pointers to requests
 */
struct me_request *campaign_simple_request[] =
{
  &(struct me_request)
  {
    .id = 0xB0000001, .name = "Test request action function that succeess",
    .action = campaign_simple_0xB0000001_request_action
  },
  &(struct me_request)
  {
    .id = 0xB0000002, .name = "Test request action function that fails",
    .action = campaign_simple_0xB0000002_request_action
  },
  NULL
};

#undef YCP_NAME
