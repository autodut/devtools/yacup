/* setup.c - Setup definitions for *simple* measurements campaign
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/campaign/simple/setup"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Setups (Whatever is needed before to measure)
 */
/* Test setup action function that succeess */
int campaign_simple_0xA0000001_setup_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_simple_0xA0000001_setup_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Setup is executed and will exit with Ok\n");
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Test setup action function that fails */
int campaign_simple_0xA0000002_setup_action(struct me *me, size_t m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "campaign_simple_0xA0000002_setup_action"

  (void)me;
  (void)m_idx;

  /* Cya! */
  _dbg("Setup is executed and will exit with Error\n");
  return 1;

  /* Free _dbg() config */
  #undef YCP_FNAME
}
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
 * Array of pointers to setups
 */
struct me_setup *campaign_simple_setup[] =
{
  &(struct me_setup)
  {
    .id = 0xA0000001, .name = "Test setup action function that succeess",
    .action = campaign_simple_0xA0000001_setup_action
  },
  &(struct me_setup)
  {
    .id = 0xA0000002, .name = "Test setup action function that fails",
    .action = campaign_simple_0xA0000002_setup_action
  },
  NULL
};

#undef YCP_NAME
