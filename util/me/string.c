/* string.c - All `me` string-related variables and functions
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include "yacup/me.h"
#include "yacup/me/string.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/string"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_string
 * @{
 */

/**
 * @name Exported strings
 * @brief      Tipically used in conjunction with @ref me_sprint_criteria
 * @{
 */

/**
 * @brief      Available @ref me_state strings
 * @note       Default string that will be used if none is selected. As it is
 *             exported, it can be used at will
 */
char *me_state_str[] =
{
  /* ME_STA_NOT_DONE = 0x00 */
  "Not done",
  /* ME_STA_DONE     = 0x01 */
  "Done",
  /* ME_STA_SKIPPED  = 0x02 */
  "Skipped",
  /* ME_STA_ERROR    = 0x03 */
  "Error"
};

/**
 * @brief      Available @ref me_relation operator strings, short/symbol version
 * @note       Default string that will be used if none is selected. As it is
 *             exported, it can be used at will
 */
char *me_relation_str[] =
{
  /* ME_REL_ANYTHING         = 0x00 */
  "not constrained",
  /* ME_REL_EQUAL            = 0x01 */
  "=",
  /* ME_REL_NOT_EQUAL        = 0x02 */
  "!=",
  /* ME_REL_GREATER          = 0x03 */
  ">",
  /* ME_REL_LESS             = 0x04 */
  "<",
  /* ME_REL_GREATER_OR_EQUAL = 0x05 */
  ">=",
  /* ME_REL_LESS_OR_EQUAL    = 0x06 */
  "<="
};

/**
 * @brief      Available @ref me_relation operator strings, long/words version
 * @note       Default string that will be used if none is selected. As it is
 *             exported, it can be used at will
 */
char *me_relation_long_str[] =
{
  /* ME_REL_ANYTHING         = 0x00 */
  "not constrained",
  /* ME_REL_EQUAL            = 0x01 */
  "equal to",
  /* ME_REL_NOT_EQUAL        = 0x02 */
  "not equal to",
  /* ME_REL_GREATER          = 0x03 */
  "greater than",
  /* ME_REL_LESS             = 0x04 */
  "less than",
  /* ME_REL_GREATER_OR_EQUAL = 0x05 */
  "greater than or equal to",
  /* ME_REL_LESS_OR_EQUAL    = 0x06 */
  "less than or equal to"
};

/**
 * @brief      Available @ref me_logic operator strings
 * @note       Default string that will be used if none is selected. As it is
 *             exported, it can be used at will
 */
char *me_logic_str[] =
{
  /* ME_LOG_NONE = 0x00 */
  "none",
  /* ME_LOG_AND  = 0x01 */
  "and",
  /* ME_LOG_OR   = 0x02 */
  "or"
};

/**
 * @brief      Available @ref me_attachment type strings
 * @note       Default string that will be used if none is selected. As it is
 *             exported, it can be used at will
 */
char *me_attachment_str[] =
{
  /* ME_ATT_NO     = 0x00 */
  "No",
  /* ME_ATT_CUSTOM = 0x01 */
  "Custom",
  /* ME_ATT_ARRAY  = 0x02 */
  "Array of double elements",
  /* ME_ATT_PNG    = 0x03 */
  "PNG Image",
  /* ME_ATT_JPG    = 0x04 */
  "JPG Image",
  /* ME_ATT_BMP    = 0x05 */
  "BMP Image"
};

/** @} */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Create human-like criteria string using *sprintf* approach
 * Read `yacup/me/string.h` for complete information. */
int me_sprint_criteria(char *str, struct me *me, uint64_t id,
                       char *relation_str[], char *logic_str[])
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_sprint_criteria"

  size_t m_idx = 0;
  size_t p_idx = 0;
  int s_len = 0;

  /* Cannot locate it? (Validated here, partial allowed) */
  if (me_locate_meas_by_id(me, id, &m_idx))
  {
    _dbg("Invalid me or setup, measure or measurement arrays\n");
    return -s_len;
  }

  /* Ensure valid strings */
  relation_str = (relation_str == NULL)?me_relation_str:relation_str;
  logic_str = (logic_str == NULL)?me_logic_str:logic_str;

  /* Iterate all conditions found */
  for (p_idx = 0; p_idx < ME_PASS_CHECKS; p_idx++)
  {
    if (me->meas[m_idx]->pass_if[p_idx].type == ME_REL_ANYTHING)
    {
      /* The special cases */
      s_len += sprintf(str + s_len, "%s",
           relation_str[me->meas[m_idx]->pass_if[p_idx].type]);
    }
    else
    {
      /* All cases following 'Symbol is <equal/less than> <value>' */
      s_len += sprintf(str + s_len, "%s %g %s",
           relation_str[me->meas[m_idx]->pass_if[p_idx].type],
           me->meas[m_idx]->pass_if[p_idx].reference,
           me->meas[m_idx]->unit);
    }

    if (me->meas[m_idx]->pass_if[p_idx].next == ME_LOG_NONE)
    {
      /* Stop here */
      break;
    }
    else
    {
      /* One more condition */
      s_len += sprintf(str + s_len, " %s ",
      logic_str[me->meas[m_idx]->pass_if[p_idx].next]);
    }
  }

  /* Return used characters */
  return s_len;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/** @} */

#undef YCP_NAME
