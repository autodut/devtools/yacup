/* file.c - All `me` file-related variables and functions
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include "yacup/me.h"
#include "yacup/me/file.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/file"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Uses a char array and pre-fill with campaign base folder (slash-ended)
 * Read `yacup/me/file.h` for complete information. */
char * me_campaign_path(struct me *me, int *f_len)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_campaign_path"

  /* Reserved here for ease of use */
  static char f_str[ME_LEN_FILE_PATH + ME_LEN_FILE_NAME + 1] = { 0x00 };
  struct stat st = { 0 };

  /* Invalid *me* (partial accepted here) ? */
  if (me_validate(me) > 0 ||
      /* Valid size holder? */
      (f_len == NULL))
  {
    _dbg("Invalid 'me' or measurement array\n");
    *f_len = 0;
    return NULL;
  }

  /* Start composing the path with the folder */
  *f_len = sprintf(f_str, "%s", me->folder);

  /* Ensure trailing slash */
  if (f_str[*f_len - 1] != '/')
  {
    *f_len += sprintf(&f_str[*f_len], "%s", "/");
  }

  /* Create folder if not exists */
  if (stat(f_str, &st) == -1)
  {
    _dbg("Folder '%s' not found. Creating it\n", f_str);
    mkdir(f_str, 0755);
  }

  /* Let's go! */
  return f_str;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Get the path to the file used by a driver
 * Read `yacup/me/file.h` for complete information. */
char * me_file_path(struct me *me, uint64_t id,
                    struct me_file_driver *driver, char *suffix)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_file_path"

  size_t idx = 0;
  int f_len = 0;
  char *f_str = NULL;

  if (/* Cannot validate, even partially? */
      (me_validate(me) > 0) ||
      /* Invalid driver? */
      (driver == NULL) ||
      /* Invalid driver path composer? */
      (driver->path == NULL))
  {
    _dbg("Invalid 'me', measurement array or file driver\n");
    return NULL;
  }

  /* Get a string with campaign folder */
  f_str = me_campaign_path(me, &f_len);

  /* Measurement is not mandatory at this step, so provides it if found */
  if ((f_len = driver->path(f_str, f_len, me,
                   me_locate_meas_by_id(me, id, &idx)?NULL:me->meas[idx])) < 0)
  {
    _dbg("Cannot create the path using '%s' driver\n", driver->name);
    return NULL;
  }

  /* Add suffix if present */
  if (suffix != NULL)
  {
    f_len += sprintf(f_str + f_len, "%s", suffix);
  }

  /* Let's go! */
  return f_str;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Save a measurement (to a file or whatever the driver decides)
 * Read `yacup/me/file.h` for complete information. */
int me_save(struct me *me, uint64_t id, struct me_file_driver *driver)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_save"

  size_t idx = 0;

  if (/* Cannot validate, even partially? */
      (me_validate(me) > 0) ||
      /* Invalid driver? */
      (driver == NULL) ||
      /* Invalid driver save operation? */
      (driver->save == NULL))
  {
    _dbg("Invalid 'me', measurement array or file driver\n");
    return 1;
  }

  /* Measurement is not mandatory at this step, so provides it if found */
  if (me_locate_meas_by_id(me, id, &idx))
  {
    /* Let's go! */
    return driver->save(driver, me, NULL);
  }
  else
  {
    /* Add campaign if not present */
    if (me->meas[idx]->campaign == 0)
    {
      me->meas[idx]->campaign = me->id;
    }

    /* Add timestamp if not present */
    if ((me->meas[idx]->timestamp == 0) &&
        (me_timestamp_ms(&me->meas[idx]->timestamp)))
    {
      _dbg("Cannot set a valid timestamp for this measurement\n");
    }

    /* Let's go! */
    return driver->save(driver, me, me->meas[idx]);
  }

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Save a measurement (to a file or whatever the driver decides)
 * Read `yacup/me/file.h` for complete information. */
int me_load(struct me *me, uint64_t id, struct me_file_driver *driver)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_load"

  size_t idx = 0;

  if (/* Cannot validate, even partially? */
      (me_validate(me) > 0) ||
      /* Invalid driver? */
      (driver == NULL) ||
      /* Invalid driver load operation? */
      (driver->load == NULL))
  {
    _dbg("Invalid 'me', measurement array or file driver\n");
    return 1;
  }

  /* Measurement is not mandatory at this step, so provides it if found */
  if (driver->load(driver, me,
                   me_locate_meas_by_id(me, id, &idx)?NULL:me->meas[idx]))
  {
    _dbg("Cannot load using '%s' driver\n", driver->name);
    return 1;
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
