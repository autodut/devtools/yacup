/* me.c - Measurement engine API for yacup project
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <time.h>
#include "yacup/me.h"
#include "yacup/me/file.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/me"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Validates if a `me` is fully valid, partially valid or invalid at all
 * Read `yacup/me.h` for complete information. */
int me_validate(struct me *me)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_validate"

  if (/* Invalid me? */
      (me == NULL) ||
      /* Invalid measurement? */
      (me->meas == NULL))
  {
    _dbg("Invalid 'me' or measurements array\n");
    return 1;
  }

  /* As we have a structure and a measurement array, at least we are partial */

  /* Set a name if there is not */
  if (me->name == NULL)
  {
    me->name = "TUC! The Unnamed Campaign";
  }

  /* Set an ID if there is not */
  if (me->id == 0)
  {
    me->id = 0xDEAF8055DEADBEEF;
  }

  /* Set a folder if there is not */
  if (me->folder == NULL)
  {
    me->folder = "DEAF8055DEADBEEF";
  }

  if (/* Invalid setup? */
      (me->setup == NULL) ||
      /* Invalid request? */
      (me->request == NULL))
  {
    _dbg("Partially valid me '0x%08lX': '%s'\n", me->id, me->name);
    return -1;
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Initializes a measurement engine/campaign referenced by a `me` pointer
 * Read `yacup/me.h` for complete information. */
int me_init(struct me *me)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_init"

  size_t idx = 0;

  /* Invalid *me*? */
  if (/** @todo Finish 'partial' validity */
      me_validate(me))
  {
    _dbg("Invalid 'me' or setup, request or measurement arrays\n");
    return 1;
  }

  _dbg("Initializing '%s'\n", me->name);

  /* Lets check each setup element */
  _dbg("- Setups found:\n");
  for (idx = 0; me->setup[idx] != NULL; idx++)
  {
    _dbg("  + 0x%08lX - '%s'\n",
         me->setup[idx]->id,
         me->setup[idx]->name);
  }

  /* Lets check each request element */
  _dbg("- Requests found:\n");
  for (idx = 0; me->request[idx] != NULL; idx++)
  {
    _dbg("  + 0x%08lX - '%s'\n",
         me->request[idx]->id,
         me->request[idx]->name);
  }

  /* Lets check each measurement element */
  _dbg("- Measurements found:\n");
  for (idx = 0; me->meas[idx] != NULL; idx++)
  {
    _dbg("  + 0x%08lX (0x%08lX:0x%08lX) - '%s' > '%s'\n",
         me->meas[idx]->id,
         me->meas[idx]->setup,
         me->meas[idx]->request,
         me->meas[idx]->location,
         me->meas[idx]->name);
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Create a valid timestamp (epoch in ms)
 * Read `yacup/me.h` for complete information. */
int me_timestamp_ms(uint64_t *timestamp)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_timestamp_ms"

  *timestamp = (uint64_t)time(NULL);
  *timestamp *= 1000;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Locate a measurement inside a campaign by its id
 * Read `yacup/me.h` for complete information. */
int me_locate_meas_by_id(struct me *me, uint64_t id, size_t *m_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_locate_meas_by_id"

  /* Invalid me? */
  if (/* Partial is allowed here as we work only with measurements list */
      (me_validate(me) > 0) ||
      /* Invalid m_idx? */
      (m_idx == NULL))
  {
    _dbg("Invalid me, index or setup, measure or measurement arrays\n");
    return 1;
  }

  /* Search by id */
  for (*m_idx = 0; me->meas[*m_idx] != NULL; (*m_idx)++)
  {
    if (id == me->meas[*m_idx]->id)
    {
      /* Found! Nice, get out this loop */
      break;
    }
  }

  /* If found, this will not be true */
  if (me->meas[*m_idx] == NULL)
  {
    _dbg("Measurement '0x%08lX' not found\n", id);
    return 1;
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Locate a setup inside a campaign by its id
 * Read `yacup/me.h` for complete information. */
int me_locate_setup_by_id(struct me *me, uint64_t id, size_t *s_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_locate_setup_by_id"

  /* Invalid me? */
  if (/* Full needed here */
      me_validate(me) ||
      /* Invalid s_idx? */
      (s_idx == NULL))
  {
    _dbg("Invalid me, index or setup, measure or measurement arrays\n");
    return 1;
  }

  /* Search by id */
  for (*s_idx = 0; me->setup[*s_idx] != NULL; (*s_idx)++)
  {
    if (id == me->setup[*s_idx]->id)
    {
      /* Found! Nice, get out this loop */
      break;
    }
  }

  /* If found, this will not be true */
  if (me->setup[*s_idx] == NULL)
  {
    _dbg("Setup '0x%08lX' not found\n", id);
    return 1;
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Locate a request inside a campaign by its id
 * Read `yacup/me.h` for complete information. */
int me_locate_request_by_id(struct me *me, uint64_t id, size_t *r_idx)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_locate_request_by_id"

  /* Invalid me? */
  if (/* Full needed here */
      me_validate(me) ||
      /* Invalid r_idx? */
      (r_idx == NULL))
  {
    _dbg("Invalid me, index or setup, request or measurement arrays\n");
    return 1;
  }

  /* Search by id */
  for (*r_idx = 0; me->request[*r_idx] != NULL; (*r_idx)++)
  {
    if (id == me->request[*r_idx]->id)
    {
      /* Found! Nice, get out this loop */
      break;
    }
  }

  /* If found, this will not be true */
  if (me->request[*r_idx] == NULL)
  {
    _dbg("Request '0x%08lX' not found\n", id);
    return 1;
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Evaluates a logical expression using an operator and 2 operands
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | False            |
 *             | `== 1` | True             |
 */
static int me_eval_logic(enum me_logic l_oper, int l_cond, int r_cond)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_eval_logic"

  switch (l_oper)
  {
    /* No logical operation, return left operand */
    case ME_LOG_NONE:
      return (l_cond)?1:0;
      break;
    /* Left AND Right operands have to be true */
    case ME_LOG_AND:
      return (l_cond && r_cond)?1:0;
      break;
    /* Left OR Right operands have to be true */
    case ME_LOG_OR:
      return (l_cond || r_cond)?1:0;
      break;
  }
  /* Cannot ensure a true, so false */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Evaluates all applicable measurement conditions
 * Read `yacup/me.h` for complete information. */
int me_eval_meas(struct me *me, uint64_t id, int *pass)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_eval_meas"

  size_t idx = 0;
  size_t p_idx = 0;
  int p_pass = 0;

  /* Cannot locate it? (Validated here, partial allowed) */
  if (me_locate_meas_by_id(me, id, &idx))
  {
    _dbg("Invalid 'me' or setup, measure or measurement arrays\n");
    return 1;
  }

  /* Default is to fail */
  *pass = 0;

  /* Iterate all valid pass-checks */
  for (p_idx = 0; p_idx < ME_PASS_CHECKS; p_idx++)
  {
    switch (me->meas[idx]->pass_if[p_idx].type)
    {
      /* Default. Undefined. It will always validate */
      case ME_REL_ANYTHING:
        p_pass = 1;
        break;

      /* Value have to be equal to the reference */
      case ME_REL_EQUAL:
        p_pass = (me->meas[idx]->value ==
                  me->meas[idx]->pass_if[p_idx].reference)?1:0;
        break;

      /* Value have to be different than the reference */
      case ME_REL_NOT_EQUAL:
        p_pass = (me->meas[idx]->value !=
                  me->meas[idx]->pass_if[p_idx].reference)?1:0;
        break;

      /* Value have to be greater than the reference */
      case ME_REL_GREATER:
        p_pass = (me->meas[idx]->value >
                  me->meas[idx]->pass_if[p_idx].reference)?1:0;
        break;

      /* Value have to be less than the reference */
      case ME_REL_LESS:
        p_pass = (me->meas[idx]->value <
                  me->meas[idx]->pass_if[p_idx].reference)?1:0;
        break;

      /* Value have to be greater than or equal to the reference */
      case ME_REL_GREATER_OR_EQUAL:
        p_pass = (me->meas[idx]->value >=
                  me->meas[idx]->pass_if[p_idx].reference)?1:0;
        break;

      /* Value have to be less than or equal to the reference */
      case ME_REL_LESS_OR_EQUAL:
        p_pass = (me->meas[idx]->value <=
                  me->meas[idx]->pass_if[p_idx].reference)?1:0;
        break;

      /* Dunno man, so false */
      default:
        p_pass = 0;
        break;
    }

    /* Single or multiple condition? */
    if (p_idx == 0)
    {
      *pass = p_pass;
    }
    else
    {
      *pass = me_eval_logic(me->meas[idx]->pass_if[p_idx - 1].next,
                            *pass, p_pass);
    }

    /* Is this the last one? */
    if (me->meas[idx]->pass_if[p_idx].next == ME_LOG_NONE)
    {
      break;
    }
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Performs a measurement (setup -> request -> save new measurement value)
 * Read `yacup/me.h` for complete information. */
int me_perform_meas(struct me *me, uint64_t id, struct me_user_cb *cb)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_perform_meas"

  size_t overwrite_old = 0;
  size_t retry_count = 0;
  int retry_res = 0;

  /* Invalid me? */
  if (me_validate(me))
  {
    _dbg("Invalid me, index or setup, request or measurement arrays\n");
    return 1;
  }

  /* Can locate the measurement? */
  if (me_locate_meas_by_id(me, id, &me->last_meas_idx))
  {
    _dbg("Cannot locate the measurement\n");
    return 1;
  }

  /* Can locate the setup? */
  if (me_locate_setup_by_id(me,
                            me->meas[me->last_meas_idx]->setup,
                            &me->last_setup_idx))
  {
    _dbg("Cannot locate the setup\n");
    return 1;
  }

  /* Can locate the request? */
  if (me_locate_request_by_id(me,
                              me->meas[me->last_meas_idx]->request,
                              &me->last_request_idx))
  {
    _dbg("Cannot locate the request\n");
    return 1;
  }

  /* Now we let track of this call, let's see if is repeated or not */
  if ((me->repeat_measurement != 1) &&
      (me->meas[me->last_meas_idx]->state == ME_STA_DONE))
  {
    /* Already taken, we will not overwrite it, so pacefully exit now */
    _dbg("Skipping measurement (Already done and not configured to repeat)\n");
    return 0;
  }

  /* So we have a measurement to work with, lets prepare the setup */
  if ((cb != NULL) && (cb->setup_prepare != NULL))
  {
    if (cb->setup_prepare(me, me->last_meas_idx))
    {
      _dbg("Error when calling the setup preparation callback\n");
      return 1;
    }
  }

  /* Set the current state */
  me->meas[me->last_meas_idx]->state = ME_STA_NOT_DONE;

  /* Set the current campaign */
  me->meas[me->last_meas_idx]->campaign = me->id;

  /* Add timestamp */
  if ((me->meas[me->last_meas_idx]->timestamp == 0) &&
      (me_timestamp_ms(&me->meas[me->last_meas_idx]->timestamp)))
  {
    _dbg("Cannot set a valid timestamp for this measurement\n");

    /* Set the current state */
    me->meas[me->last_meas_idx]->state = ME_STA_ERROR;
    return 1;
  }

  if ((me->repeat_setup == 1) ||
      (me->last_setup_bck != me->setup[me->last_setup_idx]))
  {
    /* Now call the setup itself */
    retry_count = 0;
    while ((me->setup[me->last_setup_idx]->action != NULL) &&
           (retry_count < ME_MAX_RETRIES))
    {
      /* It succeeded? */
      if (me->setup[me->last_setup_idx]->action(me, me->last_meas_idx) == 0)
      {
        /* Yup! */
        break;
      }

      /* No, do we have a callback to for retries? */
      if ((cb != NULL) && (cb->setup_retry != NULL))
      {
        retry_res = cb->setup_retry(me, me->last_meas_idx);
        if (retry_res > 0)
        {
          /* Retry function doesn't wanna live anymore */
          _dbg("Setup retry callback requested to stop\n");

          /* Set the current state */
          me->meas[me->last_meas_idx]->state = ME_STA_ERROR;
          return 1;
        }
        else if (retry_res < 0)
        {
          /* Retry function wants to skip */
          _dbg("Setup retry callback requested to skip\n");
          break;
        }
      }
      retry_count++;
    }

    /* Do we tried a lot? */
    if (retry_count == ME_MAX_RETRIES)
    {
      /* Otherwise, fail */
      _dbg("Setup failed and retries exhausted\n");

      /* Set the current state */
      me->meas[me->last_meas_idx]->state = ME_STA_ERROR;
      return 1;
    }
  }
  else
  {
    /* Configured to not repeat, and current one is same as previous. Skip */
    _dbg("Skipping setup. Configured to avoid setup repetitions\n");
  }

  /* Update last setup */
  me->last_setup_bck = me->setup[me->last_setup_idx];

  /* Now repeat with the request */
  if ((cb != NULL) && (cb->request_prepare != NULL))
  {
    if (cb->request_prepare(me, me->last_meas_idx))
    {
      _dbg("Error when calling the request preparation callback\n");

      /* Set the current state */
      me->meas[me->last_meas_idx]->state = ME_STA_ERROR;
      return 1;
    }
  }

  /* Now call the request itself */
  retry_count = 0;
  while ((me->request[me->last_request_idx]->action != NULL) &&
         (retry_count < ME_MAX_RETRIES))
  {
    /* It succeeded? */
    if (me->request[me->last_request_idx]->action(me, me->last_meas_idx) == 0)
    {
      /* Set the final state (to fail when saving is not a measurement error) */
      me->meas[me->last_meas_idx]->state = ME_STA_DONE;

      /* Yup! */
      break;
    }

    /* No, do we have a callback to for retries? */
    if ((cb != NULL) && (cb->request_retry != NULL))
    {
      retry_res = cb->request_retry(me, me->last_meas_idx);
      if (retry_res > 0)
      {
        /* Retry function doesn't wanna live anymore */
        _dbg("Request retry callback requested to stop\n");

        /* Set the current state */
        me->meas[me->last_meas_idx]->state = ME_STA_ERROR;
        return 1;
      }
      else if (retry_res < 0)
      {
        /* Retry function wants to skip */
        _dbg("Request retry callback requested to skip\n");

        /* Set the current state */
        me->meas[me->last_meas_idx]->state = ME_STA_SKIPPED;
        break;
      }
    }
    retry_count++;
  }

  /* Do we tried a lot? */
  if (retry_count == ME_MAX_RETRIES)
  {
    /* Otherwise, fail */
    _dbg("Request failed and retries exhausted\n");

    /* Set the current state */
    me->meas[me->last_meas_idx]->state = ME_STA_ERROR;
    return 1;
  }

  /* Do we have auto-save capabilities enabled? */
  if ((me->file_driver == NULL))
  {
    /* Nope, so we gracefully exit now */
    return 0;
  }

  /* And finally repeat with the save */
  if ((cb != NULL) && (cb->save_prepare != NULL))
  {
    if (cb->save_prepare(me, me->last_meas_idx))
    {
      _dbg("Error when calling the save preparation callback\n");
      return 1;
    }
  }

  /* Now call the save itself */
  retry_count = 0;
  while (retry_count < ME_MAX_RETRIES)
  {
    /* If we did a repeat, we need to overwrite */
    overwrite_old = me->file_driver->overwrite;
    if ((me->repeat_measurement == 1) && (overwrite_old == 0))
    {
      _dbg("Forcing file overwrite due to repeated measurement\n");
      me->file_driver->overwrite = 1;
    }

    /* It succeeded? */
    if (me_save(me, id, me->file_driver) <= 0)
    {
      /* Restore overwrite bit as soon as possible */
      if ((me->repeat_measurement == 1) && (overwrite_old == 0))
      {
        me->file_driver->overwrite = overwrite_old;
        _dbg("Driver overwrite bit restored to '%lu'\n", overwrite_old);
      }

      /* Yup! */
      break;
    }

    /* Restore overwrite bit as soon as possible */
    if ((me->repeat_measurement == 1) && (overwrite_old == 0))
    {
      me->file_driver->overwrite = overwrite_old;
      _dbg("Driver overwrite bit restored to '%lu'\n", overwrite_old);
    }

    /* No, do we have a callback to for retries? */
    if ((cb != NULL) && (cb->save_retry != NULL))
    {
      retry_res = cb->save_retry(me, me->last_meas_idx);
      if (retry_res > 0)
      {
        /* Retry function doesn't wanna live anymore */
        _dbg("Save retry callback requested to stop\n");
        return -1;
      }
      else if (retry_res < 0)
      {
        /* Retry function wants to skip */
        _dbg("Save retry callback requested to skip\n");
        break;
      }
    }
    retry_count++;
  }

  /* Do we tried a lot? */
  if (retry_count == ME_MAX_RETRIES)
  {
    /* Otherwise, fail */
    _dbg("Save failed and retries exhausted\n");
    return -1;
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
