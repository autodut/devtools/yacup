/* debug.c - Debug functions to ease `me` development flow
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include "yacup/me.h"
#include "yacup/me/string.h"
#include "yacup/me/file.h"
#include "yacup/me/debug.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#define YCP_FORCE_DEBUG
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/debug"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Print `me_meas` information to STDOUT.
 * Read `yacup/me/debug.h` for complete information. */
void me_print_meas_info(struct me *me, uint64_t id)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_print_meas_info"

  size_t meas_idx = 0;
  size_t s_idx = 0;
  size_t r_idx = 0;
  size_t idx = 0;
  int pass = 0;

  /* Cannot locate it? */
  if (me_locate_meas_by_id(me, id, &meas_idx))
  {
    _dbg("Invalid me or setup, request or measurement arrays\n");
    return;
  }

  _dbg("Printing info about measurement id '0x%08lX':\n",
                                                       me->meas[meas_idx]->id);
  _dbg("- Timestamp ......: %lu s (EPOCH)\n",
                                         me->meas[meas_idx]->timestamp / 1000);
  _dbg("- Location/Net ...: %s\n",
                                                 me->meas[meas_idx]->location);
  _dbg("- Name ...........: %s\n",
                                                     me->meas[meas_idx]->name);
  _dbg("- Symbol .........: %s\n",
                                                   me->meas[meas_idx]->symbol);
  _dbg("- Value ..........: %g %s\n",
                          me->meas[meas_idx]->value, me->meas[meas_idx]->unit);
  _dbg("- Temperature ....: %g ºC\n",
                                              me->meas[meas_idx]->temperature);
  _dbg("- Humidity .......: %g %%\n",
                                                 me->meas[meas_idx]->humidity);
  _dbg("- Channel ........: %lu\n",
                                                  me->meas[meas_idx]->channel);
  _dbg("- State ..........: %s\n",
                                      me_state_str[me->meas[meas_idx]->state]);
  _dbg("- Save folder ....: '%s/'\n",
                                                                   me->folder);
  _dbg("- Campaign ID ....: 0x%08lX\n",
                                                 me->meas[meas_idx]->campaign);

  if (me_eval_meas(me, id, &pass))
  {
    _dbg("! Error when evaluating the measurement\n");
  }
  _dbg("- Criteria .......: %s\n", (pass == 1)?"PASS":"NOT PASS");

  for (idx = 0; idx < ME_PASS_CHECKS; idx++)
  {
    _dbg("                  : %g must be %s %g\n",
         me->meas[meas_idx]->value,
         me_relation_str[me->meas[meas_idx]->pass_if[idx].type],
         me->meas[meas_idx]->pass_if[idx].reference
         );
    if (me->meas[meas_idx]->pass_if[idx].next == ME_LOG_NONE)
    {
      _dbg("                  : Stop\n");
      break;
    }
    else
    {
      _dbg("                  : %s\n",
           me_logic_str[me->meas[meas_idx]->pass_if[idx].next]);
    }
  }

  for (idx = 0; idx < ME_ATTACHMENTS; idx++)
  {
    _dbg("- Attachment #%lu ..: %s\n",
         idx,
         me_attachment_str[me->meas[meas_idx]->attachment[idx]]
         );
    if (me->meas[meas_idx]->attachment[idx] == ME_ATT_NO)
    {
      break;
    }
  }

  /* Cannot locate it? */
  if (me_locate_setup_by_id(me, me->meas[meas_idx]->setup, &s_idx))
  {
    _dbg("Invalid me or setup, request or measurement arrays\n");
    return;
  }
  _dbg("- Setup ID .......: 0x%08lX (%s)\n",
                            me->meas[meas_idx]->setup, me->setup[s_idx]->name);

  /* Cannot locate it? */
  if (me_locate_request_by_id(me, me->meas[meas_idx]->request, &r_idx))
  {
    _dbg("Invalid me or setup, request or measurement arrays\n");
    return;
  }
  _dbg("- Request ID .....: 0x%08lX (%s)\n",
                        me->meas[meas_idx]->request, me->request[r_idx]->name);
  return; 

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* Print `me` information to STDOUT.
 * Read `yacup/me/debug.h` for complete information. */
void me_print_info(struct me *me)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_print_info"

  size_t idx = 0;

  /* Invalid me? */
  if (me == NULL)
  {
    _dbg("Invalid me\n");
    return;
  }

  _dbg("Printing info about campaign id '0x%08lX':\n", me->id);
  for (idx = 0; me->setup[idx] != NULL; idx++) {}
  _dbg("- Setups ........: %p (Total %lu)\n", (void *)me->setup, idx);
  for (idx = 0; me->request[idx] != NULL; idx++) {}
  _dbg("- Requests ......: %p (Total %lu)\n", (void *)me->request, idx);
  for (idx = 0; me->meas[idx] != NULL; idx++) {}
  _dbg("- Measurements ..: %p (Total %lu)\n", (void *)me->meas, idx);
  _dbg("- Name ..........: %s\n", me->name);
  _dbg("- Folder ........: %s\n", me->folder);
  _dbg("- Author ........: %s\n", me->author);
  _dbg("- Version .......: v%s\n", me->version);
  _dbg("- Repeat meas ...: %s\n", (me->repeat_measurement == 1)?"Yes":"No");
  _dbg("- Repeat setup ..: %s\n", (me->repeat_setup == 1)?"Yes":"No");
  _dbg("- File driver ...: %s\n",
       (me->file_driver == NULL)?"No driver":me->file_driver->name);

  /* Let's go! */
  return; 

  /* Free _dbg() config */
  #undef YCP_FNAME
}

#undef YCP_NAME
#undef YCP_FORCE_DEBUG
