/* raw_driver_data.c - Driver for raw measurement files with metadata
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include "yacup/me.h"
#include "yacup/me/file.h"
#include "yacup/me/file/raw_meas.h"
#include "yacup/me/file/raw_driver_data.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/file/raw_driver_data"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_raw_driver_data
 * @{
 */

/**
 * @addtogroup me_file_driver_raw_driver_data_driver_ops Driver operations
 * @brief      Private, accessible through the driver variable
 * @{
 */

/**
 * @brief      Create a valid filename with path according this driver rules
 *
 * @param      str      Pointer used to save the generated string
 * @param      str_len  Length inside str where the last character was written
 * @param      me       Pointer to *me* (campaign to be based on)
 * @param      meas     Pointer to affected measurement
 *
 * @note       This function implements @ref me_file_driver::path driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     If successful, the total number of characters in `str` is
 *             returned excluding the null-character appended at the end of
 *             the string, otherwise a negative number is returned in case of
 *             failure. This allows to overwrite and append using str_len
 */
static int op_path(char *str, int str_len, struct me *me, struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_path"

  (void)me;
  int str_sz = 0;
  int str_res = 0;

  /* This is a combinated driver, so meas is optional but can be functional */
  if (meas != NULL)
  {
    if ((str_res = raw_meas.path(str, str_len, me, meas)) > 0)
    {
      str_sz = str_res;

      /* Add string and check errors */
      str_res = sprintf(str + str_sz,
                        ME_FILE_RAW_DRIVER_DATA_META_SUFFIX);
    }
  }
  else
  {
    /* Add string and check errors */
    str_res = sprintf(str + str_len,
                      ME_FILE_RAW_DRIVER_DATA_META_FILEFORMAT, me->id);
  }

  /* Errorific? */
  if (str_res <= 0)
  {
    return -1;
  }

  /* Let's go! Return full string length (not only the written chars) */
  return (str_len + str_res + str_sz);

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Save a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::save driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
static int op_save(struct me_file_driver *driver,
                   struct me *me,
                   struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_save"

  struct stat st = { 0 };
  static FILE *file = NULL;

  /* This is a combinated driver, so meas is optional but can be functional */
  if (meas != NULL)
  {
    /* Open measurement file for binary write + append inside that folder */
    if (raw_meas.save(&raw_meas, me, meas) > 0)
    {
      _dbg("Cannot save measurement '%08lX' using raw_meas\n", meas->id);
      return 1;
    }
  }

  /* Is metadata available? (driver->data) */
  if ((driver->data == NULL) ||
      (driver->data_len == 0))
  {
    /* Let's go! */
    return 0;
  }

  /* Check overwrite is needed */
  if (driver->overwrite != 1)
  {
    if (stat(me_file_path(me,
                     (meas == NULL)?0:meas->id, driver,
                     (meas == NULL)?NULL:ME_FILE_RAW_DRIVER_DATA_META_SUFFIX),
             &st) == 0)
    {
      _dbg("Skipping save. '%s' found and we cannot overwrite\n",
           me_file_path(me,
                     (meas == NULL)?0:meas->id, driver,
                     (meas == NULL)?NULL:ME_FILE_RAW_DRIVER_DATA_META_SUFFIX));
      return -1;
    }
  }

  /* Yup! Save it */
  if ((file = fopen(me_file_path(me,
                     (meas == NULL)?0:meas->id, driver,
                     (meas == NULL)?NULL:ME_FILE_RAW_DRIVER_DATA_META_SUFFIX),
                    "wb+")) == NULL)
  {
    _dbg("Cannot open %s file for writing\n",
         (meas == NULL)?"campaign":"measurement");
    return 1;
  }

  /* Save the data. CAUTION: Memory/disk layout can change between systems */
  if (fwrite(driver->data, sizeof(struct raw_driver_metadata), 1, file) != 1)
  {
    _dbg("Cannot write %s metadata into file\n",
         (meas == NULL)?"campaign":"measurement");
    return 1;
  }
  _dbg("%s saved as: '%s'\n",
       (meas == NULL)?"Campaign":"Measurement",
       me_file_path(me,
                     (meas == NULL)?0:meas->id, driver,
                     (meas == NULL)?NULL:ME_FILE_RAW_DRIVER_DATA_META_SUFFIX));

  /* Close */
  fclose(file);
  file = NULL;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Load a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::load driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
static int op_load(struct me_file_driver *driver,
                   struct me *me,
                   struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_load"

  static FILE *file = NULL;

  /* This is a combinated driver, so meas is optional but can be functional */
  if (meas != NULL)
  {
    /* Open measurement file for binary write + append inside that folder */
    if (raw_meas.load(&raw_meas, me, meas) > 0)
    {
      _dbg("Cannot load measurement '%08lX' using raw_meas\n", meas->id);
      return 1;
    }
  }

  /* Is metadata available? (driver->data) */
  if ((driver->data == NULL) ||
      (driver->data_len == 0))
  {
    /* Let's go! */
    return 0;
  }

  /* Open measurement file for read */
  _dbg("Loading %s: '%s'\n",
       (meas == NULL)?"campaign":"measurement",
       me_file_path(me,
                    (meas == NULL)?0:meas->id, driver,
                    (meas == NULL)?NULL:ME_FILE_RAW_DRIVER_DATA_META_SUFFIX));
  if ((file = fopen(me_file_path(me,
                    (meas == NULL)?0:meas->id, driver,
                    (meas == NULL)?NULL:ME_FILE_RAW_DRIVER_DATA_META_SUFFIX),
                    "r")) == NULL)
  {
    _dbg("Cannot open %s file for reading\n",
         (meas == NULL)?"campaign":"measurement");
    return 1;
  }

  /* Grab the data. CAUTION: Memory/disk layout can change between systems */
  if (fread(driver->data, sizeof(struct raw_driver_metadata), 1, file) != 1)
  {
    _dbg("Cannot read %s from file\n",
         (meas == NULL)?"campaign":"measurement");
    return 1;
  }

  /* Close test */
  fclose(file);
  file = NULL;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/** @} */

/**
 * @name Driver
 * @brief      To use with @ref me_file_path, @ref me_save and @ref me_load
 * @{
 */

/**
 * @brief      Driver-specific requirements
 * @showinitializer
 */
static struct raw_driver_metadata driver_data =
{
  .operator_name      = "John Smith",
  .sim_host           = "sim.domain.com",
  .sim_port           = "22",
  .ssh_fingerprint    = "SHA256:aDmtFmXwOBTPYpBSPWDqSZCGK1+NIIUeStha4UEnb8I",
  .ssh_priv_id_rsa    = "-----BEGIN OPENSSH PRIVATE KEY-----\n"
     "b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn\n"
     "NhAAAAAwEAAQAAAQEA3ycqP8RFxRHrzm5ZIoAhalSGBGKxyA5Ooe7aPa8+uqacssI4vWEU\n"
     "uP8hvt3QP38Y13mhCdows+sMxfbeVSY5MwX30kpSn+05NxJvmstMc3iSsjQlhgpb6roUOv\n"
     "pE9Q2Du0FK4iCuo7rCIdSZeypiFxukoo9cDCJNaxTATKSEW4F0QTA+voYxizUK9/IPmbEK\n"
     "MBPhrvdYfQqcqF7wc+yqJ3pGEPphkNHSnmH0iOHsrhEdlcPuCtMudFCVC/3FMxlNN5l1JE\n"
     "ojkHoleIBdD6wv33iEQEueOmuFzYz6ibEipjGJreJOL3gXJo0Ma3XRiiDfWeTrvrucDdo2\n"
     "CRAj91tnDwAAA8jCAyVRwgMlUQAAAAdzc2gtcnNhAAABAQDfJyo/xEXFEevOblkigCFqVI\n"
     "YEYrHIDk6h7to9rz66ppyywji9YRS4/yG+3dA/fxjXeaEJ2jCz6wzF9t5VJjkzBffSSlKf\n"
     "7Tk3Em+ay0xzeJKyNCWGClvquhQ6+kT1DYO7QUriIK6jusIh1Jl7KmIXG6Sij1wMIk1rFM\n"
     "BMpIRbgXRBMD6+hjGLNQr38g+ZsQowE+Gu91h9CpyoXvBz7KonekYQ+mGQ0dKeYfSI4eyu\n"
     "ER2Vw+4K0y50UJUL/cUzGU03mXUkSiOQeiV4gF0PrC/feIRAS546a4XNjPqJsSKmMYmt4k\n"
     "4veBcmjQxrddGKIN9Z5Ou+u5wN2jYJECP3W2cPAAAAAwEAAQAAAQEAkrGiWmziGJyhUQDF\n"
     "Y6KrVMTfnL5sdEDDfDwSB37OU9D3LaPzvLq27q35NwmFvlgEspFoRZvDbGLV8MVKftszut\n"
     "DvirQFANDnTca2yyFdzoKynKcfC7B/oBxW9DZVYmpR8KUJXwFxDHzW5Xpmssq31Mz9O0fW\n"
     "kLtz/TKU/00fHS8PlKjH1+FAxcL9mSP9/ACNOKj8yWZk++OhAdvo2v/A2xy/m81q+UQ3y9\n"
     "ESOEn459wmgfoLXVdDYJdqjMmA/deP1Y4COSXZTFTiEQvjzAmgumNKvFA1NRw7/bRyG+HL\n"
     "H9miDcNXwQI9v3RkKRz6nJTKM48M4Qobf1OrrkBdBX8tYQAAAIABqHmdZ1BDN8VulzGQQu\n"
     "YrPeZJIjzdwd5gO3NH9AJnIGtKn3tVpD6U1+BN5eatodFYQ4JwO2JA4Oah7F9F8ECYwraO\n"
     "/2TdPqvNPZhZtZ+YBxERdozvBvJQFPZHATBBoLoStvDavCMfwMAycy+fJMprxy+jVCJ8a7\n"
     "IRAHc4gmyKxgAAAIEA/42BGH6JFHXpI4EFU/8LUcCYvUo+rK9PcDEkeqCa8XSyG5VpOr/w\n"
     "l+ZDQzE5DNiRQZ1REbKzBhpp4lMcqyJTMGdKUCo7bqgLGzXYqqScAe7A3BKFZG1IOyhoBG\n"
     "kWXmpq8L1bu3qYvbUnheVLkoJa7M59+b6GcZ5qd0+YXnT8NzcAAACBAN+LJQblLJZqfx4S\n"
     "ex+9yTxCzoCEse4wDu3TmYTFQaedjxHFnK/KXDCnKu7xT5GKhuSAr+oTA7zq9+BGB2sMk0\n"
     "8dBflav2+GuEfee4GM464WuSvlHrIJ+hsbsx9pWVlrXjG9YRGtK+RKCO/ksq4FROowE1w+\n"
       "/JTTuKY4MKXyAwrpAAAADGNpZW50aUBkcmVkZwECAwQFBg==\n"
                        "-----END OPENSSH PRIVATE KEY-----\n",
  .ssh_pub_id_rsa     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfJyo/xEXFEevOb"
     "lkigCFqVIYEYrHIDk6h7to9rz66ppyywji9YRS4/yG+3dA/fxjXeaEJ2jCz6wzF9t5VJjkzB"
     "ffSSlKf7Tk3Em+ay0xzeJKyNCWGClvquhQ6+kT1DYO7QUriIK6jusIh1Jl7KmIXG6Sij1wMI"
     "k1rFMBMpIRbgXRBMD6+hjGLNQr38g+ZsQowE+Gu91h9CpyoXvBz7KonekYQ+mGQ0dKeYfSI4"
     "eyuER2Vw+4K0y50UJUL/cUzGU03mXUkSiOQeiV4gF0PrC/feIRAS546a4XNjPqJsSKmMYmt4"
     "k4veBcmjQxrddGKIN9Z5Ou+u5wN2jYJECP3W2cP raw_driver_data@yacup\n",
  .comments           = "No comments"
};

/**
 * @brief      Raw measurement file driver
 * @details    Driver capable of saving and reading measurement data to/from
 *             files.
 * @note       This driver does provide @ref me_file_driver::load operation,
 *             so it can be used for exporting and importing tasks
 */
struct me_file_driver raw_driver_data =
{
  .name = "Raw file with optional metadata"
          "(<MEASUREMENT_ID>.meas, <MEASUREMENT_ID>.meas_meta "
          "and <CAMPAIGN_ID>_raw_driver_data.meta)",
  .path = op_path,
  .save = op_save,
  .load = op_load,
  .overwrite = 1,
  .data = &driver_data,
  .data_len = sizeof(struct raw_driver_metadata)
};

/** @} */

/** @} */

#undef YCP_NAME
