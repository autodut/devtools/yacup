/* md_campaign_slides.c - Driver for markdown campaign beamer slides files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>
#include "yacup/me.h"
#include "yacup/me/string.h"
#include "yacup/me/file.h"
#include "yacup/me/file/md_meas.h"
#include "yacup/me/file/md_campaign_slides.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/file/md_campaign_slides"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_md_campaign_slides
 * @{
 */

/**
 * @addtogroup me_file_driver_md_campaign_slides_driver_ops Driver operations
 * @brief      Private, accessible through the driver variable
 * @{
 */

/**
 * @brief      Create a valid filename with path according this driver rules
 *
 * @param      str      Pointer used to save the generated string
 * @param      str_len  Length inside str where the last character was written
 * @param      me       Pointer to *me* (campaign to be based on)
 * @param      meas     Pointer to affected measurement
 *
 * @note       This function implements @ref me_file_driver::path driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     If successful, the total number of characters in `str` is
 *             returned excluding the null-character appended at the end of
 *             the string, otherwise a negative number is returned in case of
 *             failure. This allows to overwrite and append using str_len
 */
static int op_path(char *str, int str_len, struct me *me, struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_path"

  int str_sz = 0;

  /* This is a campaign (not file) driver, so meas is not needed */
  if (meas != NULL)
  {
    _dbg("Unexpected measurement, this is a campaign driver\n");
    return 1;
  }

  /* Add string and check errors */
  str_sz = sprintf(str + str_len,
                   ME_FILE_MD_CAMPAIGN_SLIDES_FILEFORMAT, me->id);

  /* Errorific? */
  if (str_sz < 0)
  {
    return str_sz;
  }

  /* Let's go! Return full string length (not only the written chars) */
  return (str_sz + str_len);

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Save a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::save driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
static int op_save(struct me_file_driver *driver, struct me *me, struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_save"

  struct stat st = { 0 };
  static FILE *file = NULL;
  time_t timer = 0;
  struct tm *tm_info = NULL;
  static char str[ME_MD_MAX_LINE_LEN] = { 0x00 };
  size_t m_idx = 0;
  int pass = 0;

  /* This is a campaign (not file) driver, so meas is needed */
  if (meas != NULL)
  {
    _dbg("Unexpected measurement, this is a campaign driver\n");
    return 1;
  }

  /* Check overwrite is needed */
  if (driver->overwrite != 1)
  {
    if (stat(me_file_path(me, 0, driver, NULL), &st) == 0)
    {
      _dbg("Skipping save. '%s' found and we cannot overwrite\n",
           me_file_path(me, 0, driver, NULL));
      return -1;
    }
  }

  /* Open measurement file for binary write + append inside that folder */
  if ((file = fopen(me_file_path(me, 0, driver, NULL), "wb+")) == NULL)
  {
    _dbg("Cannot open output file for writing\n");
    return 1;
  }

  /* Get current time */
  timer = time(NULL);
  tm_info = gmtime(&timer);

  /* Create current time date string */
  strftime(str, ME_MD_MAX_LINE_LEN, "%Y/%m/%d", tm_info);

  /* Add YAML header */
  fprintf(file, "---\n");
  fprintf(file, "title: Detailed measurements data\n");
  fprintf(file, "subtitle: Test id %08lX\n", me->id);
  fprintf(file, "author:\n");
  fprintf(file, "  - %s\n", me->author);
  fprintf(file, "version: %s\n", me->version);
  fprintf(file, "date: 'Universidad de Sevilla - 2020'\n");
  fprintf(file, "theme: 'cern'\n");
  fprintf(file, "colortheme: 'cern'\n");
  fprintf(file, "aspectratio: 169\n");
  fprintf(file, "header-includes:\n");
  fprintf(file, "- \\usepackage{caption,setspace,cmbright}\n");
  fprintf(file, "- \\captionsetup{labelformat=empty,labelsep=none}\n");
  fprintf(file, "fontsize: 10pt\n");
  fprintf(file, "---\n");
  fprintf(file, "\n");

  /* Loop to generate each row */
  for (m_idx = 0; me->meas[m_idx] != NULL; m_idx++)
  {
    /* Evaluate to prepare 'Result' string */
    if (me_eval_meas(me, me->meas[m_idx]->id, &pass))
    {
      _dbg("Error when evaluating the measurement\n");
    }

    fprintf(file, "# Measurement id *#%08lX*\n", me->meas[m_idx]->id);
    fprintf(file, "\n");
    fprintf(file, "\\scriptsize\n");
    fprintf(file, "\\begin{table}[H]\n");
    fprintf(file, "\\begin{tabular}{"
                  "|p{1.25cm}|p{1.5cm}|p{1.5cm}|p{1.5cm}|p{1cm}|"
                  "}\n");
    fprintf(file, "\\hline\n");

    /* Identifier */
    fprintf(file, "%08lX ", me->meas[m_idx]->id);

    /* Description */
    fprintf(file, "& \\multicolumn{4}{l|}{%s} \\\\ \\hline\n",
            me->meas[m_idx]->name);

    /* Location */
    fprintf(file, " & Net/Location & %s & ", me->meas[m_idx]->location);

    /* Value */
    fprintf(file, "Value & %.2f %s \\\\ \\hline\n",
            me->meas[m_idx]->value,
            me->meas[m_idx]->unit);

    /* Acceptance criteria */
    me_sprint_criteria(str, me, me->meas[m_idx]->id,
                       md_relation_long_str, md_logic_str);
    fprintf(file,
            " & Criteria & \\multicolumn{3}{l|}{"
            "$%s$ must be %s"
            "} \\\\ \\hline\n",
            me->meas[m_idx]->symbol,
            str);

    /* Result (shown only if done) */
    fprintf(file, " & Result & %s ",
                     (me->meas[m_idx]->state == ME_STA_NOT_DONE)?"---":
                      ((pass == 1)?"PASS":"NOT PASS"));

    /* Status */
    fprintf(file, "& State & %s \\\\ \\hline\n",
            md_state_str[me->meas[m_idx]->state]);

    fprintf(file, "\\end{tabular}\n");
    fprintf(file, "\\end{table}\n");
    fprintf(file, "\n");
    fprintf(file, "---\n");
    fprintf(file, "\n");
  }

  /* Thanks slide and go */
  fprintf(file, "\n");
  fprintf(file, "**Thank you very much for your attention**\n");
  fprintf(file, "\n");

  /* Close test */
  fclose(file);
  file = NULL;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/** @} */

/**
 * @name Driver
 * @brief      To use with @ref me_file_path, @ref me_save and @ref me_load
 * @{
 */

/**
 * @brief      Markdown campaign beamer slides files driver
 * @details    Driver capable of saving measurement data to Markdown
 * @note       This driver does not provide @ref me_file_driver::load operation,
 *             so it can be used only for exporting tasks
 */
struct me_file_driver md_campaign_slides =
{
  .name = "Markdown campaign beamer slides file "
          "(<CAMPAIGN_ID>_measurements-slides.md)",
  .path = op_path,
  .save = op_save,
  .load = NULL,
  .overwrite = 1,
  .data = NULL,
  .data_len = 0
};

/** @} */

/** @} */

#undef YCP_NAME
