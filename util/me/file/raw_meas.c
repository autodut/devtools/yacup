/* raw_meas.c - Driver for raw measurement files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include "yacup/me.h"
#include "yacup/me/file.h"
#include "yacup/me/file/raw_meas.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/file/raw_meas"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_raw_meas
 * @{
 */

/**
 * @addtogroup me_file_driver_raw_meas_driver_ops Driver operations
 * @brief      Private, accessible through the driver variable
 * @{
 */

/**
 * @brief      Create a valid filename with path according this driver rules
 *
 * @param      str      Pointer used to save the generated string
 * @param      str_len  Length inside str where the last character was written
 * @param      me       Pointer to *me* (campaign to be based on)
 * @param      meas     Pointer to affected measurement
 *
 * @note       This function implements @ref me_file_driver::path driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     If successful, the total number of characters in `str` is
 *             returned excluding the null-character appended at the end of
 *             the string, otherwise a negative number is returned in case of
 *             failure. This allows to overwrite and append using str_len
 */
static int op_path(char *str, int str_len, struct me *me, struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_path"

  (void)me;
  int str_sz = 0;

  /* This is a file (not campaign) driver, so meas is needed */
  if (meas == NULL)
  {
    _dbg("This is a file driver, so it is needed a measurement to load\n");
    return -1;
  }

  /* Add string and check errors */
  str_sz = sprintf(str + str_len,
                   ME_FILE_RAW_MEAS_FILEFORMAT, meas->id);

  /* Errorific? */
  if (str_sz < 0)
  {
    return str_sz;
  }

  /* Let's go! Return full string length (not only the written chars) */
  return (str_sz + str_len);

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Save a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::save driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
static int op_save(struct me_file_driver *driver,
                   struct me *me,
                   struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_save"

  struct stat st = { 0 };
  static FILE *file = NULL;

  /* This is a file (not campaign) driver, so meas is needed */
  if (meas == NULL)
  {
    _dbg("This is a file driver, so it is needed a measurement to load\n");
    return 1;
  }

  /* Check overwrite is needed */
  if (driver->overwrite != 1)
  {
    if (stat(me_file_path(me, 0, driver, NULL), &st) == 0)
    {
      _dbg("Skipping save. '%s' found and we cannot overwrite\n",
           me_file_path(me, 0, driver, NULL));
      return -1;
    }
  }

  /* Open measurement file for binary write + append inside that folder */
  if ((file = fopen(me_file_path(me, meas->id, driver, NULL), "wb+")) == NULL)
  {
    _dbg("Cannot open measurement file for writing\n");
    return 1;
  }

  /* Save the data. CAUTION: Memory/disk layout can change between systems */
  if (fwrite(meas, sizeof(struct me_meas), 1, file) != 1)
  {
    _dbg("Cannot write measurement into file\n");
    return 1;
  }
  _dbg("Measurement saved as: '%s'\n",
       me_file_path(me, meas->id, driver, NULL));

  /* Close test */
  fclose(file);
  file = NULL;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Load a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::load driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value  | Meaning          |
 *             | :----: | :--------------- |
 *             | `== 0` | Ok               |
 *             | `!= 0` | Error            |
 */
static int op_load(struct me_file_driver *driver,
                   struct me *me,
                   struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_load"

  static FILE *file = NULL;

  /* This is a file (not campaign) driver, so meas is needed */
  if (meas == NULL)
  {
    _dbg("This is a file driver, so it is needed a measurement to load\n");
    return 1;
  }

  /* Open measurement file for read */
  _dbg("Loading measurement: '%s'\n", me_file_path(me, meas->id, driver, NULL));
  if ((file = fopen(me_file_path(me, meas->id, driver, NULL), "r")) == NULL)
  {
    _dbg("Cannot open measurement file for reading\n");
    return 1;
  }

  /* Grab the data. CAUTION: Memory/disk layout can change between systems */
  if (fread(meas, sizeof(struct me_meas), 1, file) != 1)
  {
    _dbg("Cannot read measurement from file\n");
    return 1;
  }

  /* Close test */
  fclose(file);
  file = NULL;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/** @} */

/**
 * @name Driver
 * @brief      To use with @ref me_file_path, @ref me_save and @ref me_load
 * @{
 */

/**
 * @brief      Raw measurement file driver
 * @details    Driver capable of saving and reading measurement data to/from
 *             files.
 * @note       This driver does provide @ref me_file_driver::load operation,
 *             so it can be used for exporting and importing tasks
 */
struct me_file_driver raw_meas =
{
  .name = "Raw file "
          "(<MEASUREMENT_ID>.meas)",
  .path = op_path,
  .save = op_save,
  .load = op_load,
  .overwrite = 1,
  .data = NULL,
  .data_len = 0
};

/** @} */

/** @} */

#undef YCP_NAME
