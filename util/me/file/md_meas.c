/* md_meas.c - Driver for markdown measurement files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include "yacup/me.h"
#include "yacup/me/string.h"
#include "yacup/me/file.h"
#include "yacup/me/file/md_meas.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/file/markdown_meas"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_md_meas
 * @{
 */

/**
 * @addtogroup me_file_driver_md_meas_string Exported strings
 * @brief      Tipically used in conjunction with @ref me_sprint_criteria
 * @{
 */

/**
 * @brief      Available @ref me_state Markdown-styled strings
 * @note       Used as replacement for @ref me_state_str in this driver, but
 *             exported so it can be used at will
 */
char *md_state_str[] =
{
  /* String for @ref ME_STA_NOT_DONE = 0x00 */
  "Not done",
  /* String for @ref ME_STA_DONE = 0x01 */
  "Done",
  /* String for @ref ME_STA_SKIPPED = 0x02 */
  "Skipped",
  /* String for @ref ME_STA_ERROR = 0x03 */
  "Error"
};

/**
 * @brief      Available @ref me_relation operator Markdown-styled strings,
 *             short/symbol version
 * @note       Used as replacement for @ref me_relation_str in this driver, but
 *             exported so it can be used at will
 */
char *md_relation_str[] =
{
  /* String for @ref ME_REL_ANYTHING = 0x00 */
  "not constrained",
  /* String for @ref ME_REL_EQUAL = 0x01 */
  "=",
  /* String for @ref ME_REL_NOT_EQUAL = 0x02 */
  "$\\neq$",
  /* String for @ref ME_REL_GREATER = 0x03 */
  ">",
  /* String for @ref ME_REL_LESS = 0x04 */
  "<",
  /* String for @ref ME_REL_GREATER_OR_EQUAL = 0x05 */
  "$\\geqslant$",
  /* String for @ref ME_REL_LESS_OR_EQUAL = 0x06 */
  "$\\leqslant$"
};

/**
 * @brief      Available @ref me_relation operator Markdown-styled strings,
 *             long/words version
 * @note       Used as replacement for @ref me_relation_long_str in this driver,
 *             but exported so it can be used at will
 */
char *md_relation_long_str[] =
{
  /* String for @ref ME_REL_ANYTHING = 0x00 */
  "not constrained",
  /* String for @ref ME_REL_EQUAL = 0x01 */
  "equal to",
  /* String for @ref ME_REL_NOT_EQUAL = 0x02 */
  "not equal to",
  /* String for @ref ME_REL_GREATER = 0x03 */
  "greater than",
  /* String for @ref ME_REL_LESS = 0x04 */
  "less than",
  /* String for @ref ME_REL_GREATER_OR_EQUAL = 0x05 */
  "greater than or equal to",
  /* String for @ref ME_REL_LESS_OR_EQUAL = 0x06 */
  "less than or equal to"
};

/**
 * @brief      Available @ref me_logic operator Markdown-styled strings
 * @note       Used as replacement for @ref me_logic_str in this driver, but
 *             exported so it can be used at will
 */
char *md_logic_str[] =
{
  /* String for @ref ME_LOG_NONE = 0x00 */
  "none",
  /* String for @ref ME_LOG_AND = 0x01 */
  "and",
  /* String for @ref ME_LOG_OR = 0x02 */
  "or"
};

/**
 * @brief      Available @ref me_attachment type Markdown-styled strings
 * @note       Used as replacement for @ref me_attachment_str in this driver,
 *             but exported so it can be used at will
 */
char *md_attachment_str[] =
{
  /* String for @ref ME_ATT_NO = 0x00 */
  "No attachment",
  /* String for @ref ME_ATT_CUSTOM = 0x01 */
  "Custom",
  /* String for @ref ME_ATT_ARRAY = 0x02 */
  "Array of double elements",
  /* String for @ref ME_ATT_PNG = 0x03 */
  "PNG Image",
  /* String for @ref ME_ATT_JPG = 0x04 */
  "JPG Image",
  /* String for @ref ME_ATT_BMP = 0x05 */
  "BMP Image"
};

/** @} */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Create markdown table string with campaign measurements table
 * Read `yacup/me/markdown.h` for complete information. */
int me_md_table_string(struct me *me, char *str, enum me_md_criteria crit)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_md_table_string"

  size_t m_idx = 0;
  size_t s_len = 0;
  int pass = 0;

  /* In order to make this more 'cell-ish', cells will be created one-by-one */

  s_len = 0;
  /* Print table header */
  s_len += sprintf(str + s_len, "| Identifier ");
  s_len += sprintf(str + s_len, "| Description ");
  s_len += sprintf(str + s_len, "| Location ");
  s_len += sprintf(str + s_len, "| Acceptance criteria ");
  s_len += sprintf(str + s_len, "| Result ");
  s_len += sprintf(str + s_len, "| Status ");
  s_len += sprintf(str + s_len, "|\n");

  /* Print table separator (which actually it is what creates the table) */
  s_len += sprintf(str + s_len, "| :-----: ");
  s_len += sprintf(str + s_len, "| :------ ");
  s_len += sprintf(str + s_len, "| :-----: ");
  s_len += sprintf(str + s_len, "| :------ ");
  s_len += sprintf(str + s_len, "| :-----: ");
  s_len += sprintf(str + s_len, "| :-----: ");
  s_len += sprintf(str + s_len, "|\n");

  /* Loop to generate each row */
  for (m_idx = 0; me->meas[m_idx] != NULL; m_idx++)
  {
    /* Evaluate to prepare 'Result' string */
    if (me_eval_meas(me, me->meas[m_idx]->id, &pass))
    {
      _dbg("Error when evaluating the measurement\n");
    }

    /* Identifier */
    s_len += sprintf(str + s_len, "| HW%02X.%02X ",
                     (uint8_t)(me->meas[m_idx]->id >> 20),
                     (uint8_t)(me->meas[m_idx]->id >> 8));
    /* Description */
    s_len += sprintf(str + s_len, "| %s ", me->meas[m_idx]->name);
    /* Location */
    s_len += sprintf(str + s_len, "| %s ", me->meas[m_idx]->location);
    /* Acceptance criteria */
    s_len += sprintf(str + s_len, "| ");
    s_len += me_sprint_criteria(str + s_len, me, me->meas[m_idx]->id,
               (crit == MD_LONG_CRITERIA)?md_relation_long_str:md_relation_str,
                       md_logic_str);
    s_len += sprintf(str + s_len, " ");
    /* Result (shown only if done) */
    s_len += sprintf(str + s_len, "| %s ",
                     (me->meas[m_idx]->state == ME_STA_NOT_DONE)?"---":
                      ((pass == 1)?"PASS":"NOT PASS"));
    /* Status */
    s_len += sprintf(str + s_len, "| %s ",
                     md_state_str[me->meas[m_idx]->state]);
    /* And finish */
    s_len += sprintf(str + s_len, "|\n");
  }

  /* Return used characters */
  return (int)s_len;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_md_meas_driver_ops Driver operations
 * @brief      Private, accessible through the driver variable
 * @{
 */

/**
 * @brief      Create a valid filename with path according this driver rules
 *
 * @param      str      Pointer used to save the generated string
 * @param      str_len  Length inside str where the last character was written
 * @param      me       Pointer to *me* (campaign to be based on)
 * @param      meas     Pointer to affected measurement
 *
 * @note       This function implements @ref me_file_driver::path driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     If successful, the total number of characters in `str` is
 *             returned excluding the null-character appended at the end of
 *             the string, otherwise a negative number is returned in case of
 *             failure. This allows to overwrite and append using str_len
 */
static int op_path(char *str, int str_len, struct me *me, struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_path"

  (void)me;
  int str_sz = 0;

  /* This is a file (not campaign) driver, so meas is needed */
  if (meas == NULL)
  {
    _dbg("This is a file driver, so it is needed a measurement to load\n");
    return -1;
  }

  /* Add string and check errors */
  str_sz = sprintf(str + str_len,
                   ME_FILE_MD_MEAS_FILEFORMAT, meas->id);

  /* Errorific? */
  if (str_sz < 0)
  {
    return str_sz;
  }

  /* Let's go! Return full string length (not only the written chars) */
  return (str_sz + str_len);

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Save a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::save driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
static int op_save(struct me_file_driver *driver,
                   struct me *me,
                   struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_save"

  struct stat st = { 0 };
  static FILE *file = NULL;
  int pass = 0;
  char str[ME_MD_MAX_LINE_LEN] = { 0x00 };
  size_t g_idx = 0;

  /* This is a file (not campaign) driver, so meas is needed */
  if (meas == NULL)
  {
    _dbg("This is a file driver, so it is needed a measurement to load\n");
    return 1;
  }

  /* Check overwrite is needed */
  if (driver->overwrite != 1)
  {
    if (stat(me_file_path(me, 0, driver, NULL), &st) == 0)
    {
      _dbg("Skipping save. '%s' found and we cannot overwrite\n",
           me_file_path(me, 0, driver, NULL));
      return -1;
    }
  }

  /* Open measurement file for binary write + append inside that folder */
  if ((file = fopen(me_file_path(me, meas->id, driver, NULL), "wb+")) == NULL)
  {
    _dbg("Cannot open measurement file for writing\n");
    return 1;
  }

  fprintf(file, "## Information about measurement '0x%08lX'\n", meas->id);
  fprintf(file, "\n");
  fprintf(file, "- **EPOCH Timestamp**: %lu s\n", meas->timestamp / 1000);
  fprintf(file, "- **Name**: %s\n", meas->name);
  fprintf(file, "- **Location/Net**: %s\n", meas->location);
  fprintf(file, "- **State**: %s\n", md_state_str[meas->state]);
  fprintf(file, "- **Symbol**: $%s$\n", meas->symbol);
  fprintf(file, "- **Value**: %g %s\n", meas->value, meas->unit);

  /* Print criteria information */
  if (me_eval_meas(me, meas->id, &pass))
  {
    fprintf(file, "- **Criteria**: Error evaluating the measurement\n");
  }
  else
  {
    fprintf(file, "- **Criteria**: %s\n", (pass == 1)?"PASS":"NOT PASS");
    me_sprint_criteria(str, me, meas->id, md_relation_long_str, md_logic_str);
    fprintf(file, "  + $%s$ must be %s\n", meas->symbol, str);
  }

  fprintf(file, "- **Temperature**: %g ºC\n", meas->temperature);
  fprintf(file, "- **Humidity**: %g %%\n", meas->humidity);
  fprintf(file, "- **Channel**: %lu\n", meas->channel);
  fprintf(file, "- **Save folder**: `%s/`\n", me->folder);
  fprintf(file, "- **Campaign ID**: 0x%08lX\n", meas->campaign);

  /* Print attachment information */
  fprintf(file, "- **Attachments**:%s\n",
          (meas->attachment[0] == ME_ATT_NO)?" None":" ");
  for (g_idx = 0;
       (g_idx < ME_ATTACHMENTS) && (meas->attachment[g_idx] != ME_ATT_NO);
       g_idx++)
  {
    fprintf(file, "  + %s\n", md_attachment_str[meas->attachment[g_idx]]);
  }

  /** @todo Add images if this kind of attachments are involved */

  /* Print setup information */
  g_idx = 0;
  if (me_locate_setup_by_id(me, meas->setup, &g_idx))
  {
    fprintf(file, "- **Setup**: Invalid setup\n");
  }
  else
  {
    fprintf(file, "- **Setup**: `0x%08lX`\n", meas->setup);
    fprintf(file, "  + **Name**: %s\n", me->setup[g_idx]->name);
  }

  /* Print request information */
  g_idx = 0;
  if (me_locate_request_by_id(me, meas->request, &g_idx))
  {
    fprintf(file, "- **Request**: Invalid request\n");
  }
  else
  {
    fprintf(file, "- **Request**: `0x%08lX`\n", meas->request);
    fprintf(file, "  + **Name**: %s\n", me->request[g_idx]->name);
  }
  fprintf(file, "\n");

  /* Close test */
  fclose(file);
  file = NULL;

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/** @} */

/**
 * @name Driver
 * @brief      To use with @ref me_file_path, @ref me_save and @ref me_load
 * @{
 */

/**
 * @brief      Markdown measurement file driver
 * @details    Driver capable of saving measurement data to Markdown
 * @note       This driver does not provide @ref me_file_driver::load operation,
 *             so it can be used only for exporting tasks
 */
struct me_file_driver md_meas =
{
  .name = "Markdown measurement file "
          "(<MEASUREMENT_ID>.md)",
  .path = op_path,
  .save = op_save,
  .load = NULL,
  .overwrite = 1,
  .data = NULL,
  .data_len = 0
};

/** @} */

/** @} */

#undef YCP_NAME
