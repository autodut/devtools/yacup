/* excel_campaign.c - Driver for excel campaign files
 * Copyright (C) 2020 CieNTi <cienti@cienti.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include "yacup/me.h"
#include "yacup/me/string.h"
#include "yacup/me/file.h"
#include "yacup/me/file/excel_campaign.h"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
#include "yacup/debug.h"
#undef YCP_NAME
#define YCP_NAME "util/me/file/excel_campaign"

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_excel_campaign
 * @{
 */

/* Fill a given Excel sheet with current campaign measurements data
 * Read `yacup/me/file/excel_campaign.h` for complete information. */
int me_excel_add_campaign_sheet(struct me *me, struct me_meas *meas,
                                lxw_workbook *wb, lxw_worksheet *ws)
{
  /* Configure _dbg() */
  #define YCP_FNAME "me_excel_add_campaign_sheet"

  (void)meas;

  int row = 0;
  int col = 0;
  static char c_str[ME_EXCEL_MAX_CELL_LEN] = { 0x00 };
  size_t m_idx = 0;
  int pass = 0;

  /* Prepare formats for header, pass, not pass */
  lxw_format *fm_hd = workbook_add_format(wb);
  format_set_bold(fm_hd);
  lxw_format *fm_pass = workbook_add_format(wb);
  lxw_format *fm_no_pass = workbook_add_format(wb);

  /* Set column sizes */
  worksheet_set_column(ws, 0, 0, 9, NULL);
  worksheet_set_column(ws, 1, 1, 19, NULL);
  worksheet_set_column(ws, 2, 2, 8, NULL);
  worksheet_set_column(ws, 3, 3, 60, NULL);
  worksheet_set_column(ws, 4, 4, 10, NULL);
  worksheet_set_column(ws, 5, 5, 6, NULL);

  /* Print table header */
  row = 0;
  col = 0;
  worksheet_write_string(ws, row, col++, "Identifier", fm_hd);
  worksheet_write_string(ws, row, col++, "Description", fm_hd);
  worksheet_write_string(ws, row, col++, "Location", fm_hd);
  worksheet_write_string(ws, row, col++, "Acceptance criteria", fm_hd);
  worksheet_write_string(ws, row, col++, "Result", fm_hd);
  worksheet_write_string(ws, row, col++, "Status", fm_hd);

  /* Loop to generate each row */
  for (m_idx = 0; me->meas[m_idx] != NULL; m_idx++)
  {
    /* Update cell coordinates */
    row++;
    col = 0;

    /* Evaluate to prepare 'Result' string */
    if (me_eval_meas(me, me->meas[m_idx]->id, &pass))
    {
      _dbg("Error when evaluating the measurement\n");
    }

    /* Identifier */
    snprintf(c_str, ME_EXCEL_MAX_CELL_LEN, "%08lX", me->meas[m_idx]->id);
    worksheet_write_string(ws, row, col++, c_str, NULL);

    /* Description */
    snprintf(c_str, ME_EXCEL_MAX_CELL_LEN, "%s", me->meas[m_idx]->name);
    worksheet_write_string(ws, row, col++, c_str, NULL);

    /* Location */
    snprintf(c_str, ME_EXCEL_MAX_CELL_LEN, "%s", me->meas[m_idx]->location);
    worksheet_write_string(ws, row, col++, c_str, NULL);

    /* Acceptance criteria (single-liner symbol + criteria) */
    me_sprint_criteria(c_str + snprintf(c_str, ME_EXCEL_MAX_CELL_LEN,
                                        "%s ", me->meas[m_idx]->symbol),
                       me, me->meas[m_idx]->id,
                       me_relation_long_str, NULL);
    worksheet_write_string(ws, row, col++, c_str, NULL);

    /* Result (shown only if done) */
    snprintf(c_str, ME_EXCEL_MAX_CELL_LEN, "%s",
                     (me->meas[m_idx]->state == ME_STA_NOT_DONE)?"---":
                      ((pass == 1)?"PASS":"NOT PASS"));
    worksheet_write_string(ws, row, col++, c_str,
                           (me->meas[m_idx]->state == ME_STA_NOT_DONE)?NULL:
                      ((pass == 1)?fm_pass:fm_no_pass));

    /* Status */
    snprintf(c_str, ME_EXCEL_MAX_CELL_LEN, "%s",
             me_state_str[me->meas[m_idx]->state]);
    worksheet_write_string(ws, row, col++, c_str, NULL);
  }

  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/**
 * @addtogroup me_file_driver_excel_campaign_driver_ops Driver operations
 * @brief      Private, accessible through the driver variable
 * @{
 */

/**
 * @brief      Create a valid filename with path according this driver rules
 *
 * @param      str      Pointer used to save the generated string
 * @param      str_len  Length inside str where the last character was written
 * @param      me       Pointer to *me* (campaign to be based on)
 * @param      meas     Pointer to affected measurement
 *
 * @note       This function implements @ref me_file_driver::path driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     If successful, the total number of characters in `str` is
 *             returned excluding the null-character appended at the end of
 *             the string, otherwise a negative number is returned in case of
 *             failure. This allows to overwrite and append using str_len
 */
static int op_path(char *str, int str_len, struct me *me, struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_path"

  int str_sz = 0;

  /* This is a file (not campaign) driver, so meas is not needed */
  if (meas != NULL)
  {
    _dbg("Unexpected measurement, this is a campaign driver\n");
    return 1;
  }

  /* Add string and check errors */
  str_sz = sprintf(str + str_len,
                   ME_FILE_EXCEL_CAMPAIGN_FILEFORMAT, me->id);

  /* Errorific? */
  if (str_sz < 0)
  {
    return str_sz;
  }

  /* Let's go! Return full string length (not only the written chars) */
  return (str_sz + str_len);

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/**
 * @brief      Save a file according this driver rules.
 *
 * @param      driver  Pointer to file driver involved in this operation
 * @param      me      Pointer to *me* (campaign to be based on)
 * @param      meas    Pointer to measurement to use
 *
 * @note       This function implements @ref me_file_driver::save driver
 *             operation. Read `yacup/me/file.h` for complete information.
 *
 * @return     One of:
 *             | Value   | Meaning          |
 *             | :----:  | :--------------- |
 *             | `==  0` | Ok               |
 *             | `== -1` | File exists      |
 *             | `>   0` | Error            |
 */
static int op_save(struct me_file_driver *driver,
                   struct me *me,
                   struct me_meas *meas)
{
  /* Configure _dbg() */
  #define YCP_FNAME "op_save"

  struct stat st = { 0 };
  lxw_error excel_res = LXW_NO_ERROR;
  lxw_workbook *workbook = NULL;
  lxw_worksheet *worksheet = NULL;

  /* This is a file (not campaign) driver, so meas is needed */
  if (meas != NULL)
  {
    _dbg("Unexpected measurement, this is a campaign driver\n");
    return 1;
  }

  /* Check overwrite is needed */
  if (driver->overwrite != 1)
  {
    if (stat(me_file_path(me, 0, driver, NULL), &st) == 0)
    {
      _dbg("Skipping save. '%s' found and we cannot overwrite\n",
           me_file_path(me, 0, driver, NULL));
      return -1;
    }
  }

  /* Prepare a file name and create a book */
  workbook  = workbook_new(me_file_path(me, 0, driver, NULL));
  worksheet = workbook_add_worksheet(workbook, "Measurements table");

  if (me_excel_add_campaign_sheet(me, meas, workbook, worksheet))
  {
    _dbg("Error when adding a campaign sheet to a workbook");
    return 1;
  }

  /* Close the book, saving to file */
  excel_res = workbook_close(workbook);
  if (excel_res != LXW_NO_ERROR)
  {
    _dbg("Error when creating the spreadsheet");
    return 1;
  }
  /* Let's go! */
  return 0;

  /* Free _dbg() config */
  #undef YCP_FNAME
}

/** @} */

/**
 * @name Driver
 * @brief      To use with @ref me_file_path, @ref me_save and @ref me_load
 * @{
 */

/**
 * @brief      Excel campaign file driver
 * @details    Driver capable of saving campaign measurements data to Excel
 * @note       This driver does not provide @ref me_file_driver::load operation,
 *             so it can be used only for exporting tasks
 */
struct me_file_driver excel_campaign =
{
  .name = "Excel campaign file "
          "(<MEASUREMENT_ID>_measurements-table.xlsx)",
  .path = op_path,
  .save = op_save,
  .load = NULL,
  .overwrite = 1,
  .data = NULL,
  .data_len = 0
};

/** @} */

/** @} */

#undef YCP_NAME
